Pod::Spec.new do |s|
  s.name         = "PHCommon"
  s.version      = "0.4.3"
  s.summary      = "A Collection Of Classes!!!"
  s.homepage     = "https://bitbucket.org/philology/phcommon"
  s.frameworks   = 'UIKit', 'Foundation', 'CoreGraphics'
  s.license      = {:type => 'Philology',
  					:text => <<-LICENSE 
  								Copyright (C) 2013 Philology Pty. 
  								Ltd.All rights reserved.
  								LICENSE
  					}
  s.author       = { "Ricol Wang" => "ricol.wang@philology.com.au" }
  s.source       = { :git => "https://RicolWang@bitbucket.org/philology/phcommon.git", :tag => s.version.to_s }
  s.platform     = :ios, '7.1'
  s.source_files = 'PHCommon/Classes/**/*.{h,m}'
  s.resources = 'PHCommon/*.lproj', 'PHCommon/Resource/*.bundle', 'PHCommon/Classes/**/*.xib'
  s.requires_arc = true
  s.dependency 'TTTAttributedLabel', '~> 1.7.1'
  s.dependency 'SBJson', '~> 3.1.1'
end
