//
//  ViewController.h
//  PHCustomControls
//
//  Created by Ricol Wang on 28/08/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHBasicViewController.h"
#import "CalendarMonthView.h"

@interface ViewController : PHBasicViewController

@property (weak, nonatomic) IBOutlet CalendarMonthView *calendarView;
@property (weak, nonatomic) IBOutlet UIStepper *stepperWidth;
@property (weak, nonatomic) IBOutlet UIStepper *stepperHeight;

@end
