//
//  ViewController.m
//  PHCustomControls
//
//  Created by Ricol Wang on 28/08/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <CalendarMonthViewDataSource, CalendarMonthViewDelegate>

@end

@implementation ViewController

- (id)init
{
    self = [super initWithNibName:@"ViewController" bundle:nil];
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.calendarView.dataSource = self;
    self.calendarView.delegate = self;
}

#pragma mark - CalendarMonthViewDataSource

- (BOOL)calendarMonthView:(CalendarMonthView *)montheView markForDate:(NSDate *)date
{
    return arc4random() % 2;
}

#pragma mark - CalendarMonthViewDelegate

- (void)calendarMonthView:(CalendarMonthView *)monthView didSelectDate:(NSDate *)d
{
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSLog(@"didSelectDate: %@", [df stringFromDate:d]);
}

- (void)calendarMonthView:(CalendarMonthView *)monthView monthDidChange:(NSDate *)d
{
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSLog(@"monthDidChange: %@", [df stringFromDate:d]);
}

- (IBAction)widthChanged:(id)sender
{
    [self.calendarView setNewWidth:self.stepperWidth.value andNewHeight:self.stepperHeight.value withAnimation:NO];
}

- (IBAction)heightChanged:(id)sender
{
    [self.calendarView setNewWidth:self.stepperWidth.value andNewHeight:self.stepperHeight.value withAnimation:NO];
}

- (IBAction)btnTestOnTapped:(id)sender
{
    [self.calendarView reload:NO];
}

- (IBAction)btnReloadOnTapped:(id)sender
{
    [self.calendarView reload:NO];
}

@end
