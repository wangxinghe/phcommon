//
//  AppDelegate.h
//  PHCommon
//
//  Created by Ricol Wang on 9/12/2013.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
