//
//  ImagePickerControllerWithTag.h
//  PHCustomControls
//
//  Created by Ricol Wang on 23/09/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickerControllerWithTag : UIImagePickerController

@property int tag;
@property (strong) NSObject *object;

@end
