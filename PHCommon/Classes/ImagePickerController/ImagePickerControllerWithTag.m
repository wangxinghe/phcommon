//
//  ImagePickerControllerWithTag.m
//  PHCustomControls
//
//  Created by Ricol Wang on 23/09/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import "ImagePickerControllerWithTag.h"

@interface ImagePickerControllerWithTag ()

@end

@implementation ImagePickerControllerWithTag

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
