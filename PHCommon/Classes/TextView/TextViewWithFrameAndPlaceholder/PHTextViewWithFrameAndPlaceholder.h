//
//  PHTextViewWithFrameAndPlaceholder.h
//  PHCustomControls
//
//  Created by Ricol Wang on 9/09/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import "PHTextViewWithFrame.h"

@interface PHTextViewWithFrameAndPlaceholder : PHTextViewWithFrame

@property (strong, nonatomic) NSString *strPlaceholder;
@property (strong, nonatomic) UIColor *colorPlaceholder;
@property (strong, nonatomic) UIFont *fontPlaceholder;

@end
