//
//  PHTextViewWithFrameAndPlaceholder.m
//  PHCustomControls
//
//  Created by Ricol Wang on 9/09/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import "PHTextViewWithFrameAndPlaceholder.h"

#define DELTA 9

@interface PHTextViewWithFrameAndPlaceholder ()

@property (strong) UILabel *lblPlaceholder;

- (void)InitMembersForPlaceholder;

- (void)TVTextDidChanged:(NSNotification *)notification;
- (void)TVEditDidBegin:(NSNotification *)notification;
- (void)TVEditDidEnd:(NSNotification *)notification;

@end

@implementation PHTextViewWithFrameAndPlaceholder

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    {
        // Initialization code
        [self InitMembersForPlaceholder];
    }
    return self;
}

- (id)init
{
    self = [super init];
    {
        // Initialization code
        [self InitMembersForPlaceholder];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        // Initialization code
        [self InitMembersForPlaceholder];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self InitMembersForPlaceholder];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(TVTextDidChanged:) name:UITextViewTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(TVEditDidBegin:) name:UITextViewTextDidBeginEditingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(TVEditDidEnd:) name:UITextViewTextDidEndEditingNotification object:nil];
    
    if (self.lblPlaceholder) return;
    
    self.lblPlaceholder = [[UILabel alloc] initWithFrame:CGRectMake(DELTA, DELTA, self.bounds.size.width - 2 * DELTA, self.font.lineHeight)];
    self.lblPlaceholder.text = self.strPlaceholder;
    self.lblPlaceholder.textAlignment = NSTextAlignmentLeft;
    self.lblPlaceholder.font = self.fontPlaceholder;
    self.lblPlaceholder.textColor = self.colorPlaceholder;
    self.lblPlaceholder.backgroundColor = [UIColor clearColor];

    [self addSubview:self.lblPlaceholder];
    
    //testing
//    self.lblPlaceholder.backgroundColor = [UIColor blueColor];
//    self.backgroundColor = [UIColor greenColor];
}

#pragma mark - Override Methods

- (void)setStrPlaceholder:(NSString *)strPlaceholder
{
    _strPlaceholder = strPlaceholder;
    
    if (_strPlaceholder)
    {
        self.lblPlaceholder.text = _strPlaceholder;
        self.lblPlaceholder.hidden = [_strPlaceholder isEqualToString:@""];
    }else
        self.lblPlaceholder.hidden = YES;
}

- (void)setColorPlaceholder:(UIColor *)colorPlaceholder
{
    _colorPlaceholder = colorPlaceholder;
    
    if (_colorPlaceholder)
    {
        self.lblPlaceholder.textColor = _colorPlaceholder;
    }else
        self.lblPlaceholder.textColor = [UIColor grayColor];
}

- (void)setFontPlaceholder:(UIFont *)fontPlaceholder
{
    _fontPlaceholder = fontPlaceholder;
    
    if (_fontPlaceholder)
        self.lblPlaceholder.font = _fontPlaceholder;
    else
        self.lblPlaceholder.font = [UIFont systemFontOfSize:15];
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    
    if ([self isFirstResponder]) return;
    else
        self.lblPlaceholder.hidden = !(text == nil) && ![text isEqualToString:@""];
}

#pragma mark - Private Methods

- (void)InitMembersForPlaceholder
{
    self.fontPlaceholder = self.font;
    self.colorPlaceholder = [UIColor grayColor];
    self.strPlaceholder = @"";
}

- (void)TVTextDidChanged:(NSNotification *)notification
{
    if ([self isFirstResponder])
        self.lblPlaceholder.hidden = YES;
    else
        self.lblPlaceholder.hidden = !(self.text == nil) && ![self.text  isEqualToString:@""];
}

- (void)TVEditDidBegin:(NSNotification *)notification
{
    self.lblPlaceholder.hidden = YES;
}

- (void)TVEditDidEnd:(NSNotification *)notification
{
    self.lblPlaceholder.hidden = !(self.text == nil) && ![self.text  isEqualToString:@""];
}

@end
