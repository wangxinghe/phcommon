//
//  PHTextViewWithFrame.h
//  PHCustomControls
//
//  Created by Ricol Wang on 28/08/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHTextViewWithFrame : UITextView

@property (nonatomic) float LineWidth;
@property (strong, nonatomic) UIColor *LineColor;

@end
