//
//  PHTextViewWithFrame.m
//  PHCustomControls
//
//  Created by Ricol Wang on 28/08/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import "PHTextViewWithFrame.h"
#import <QuartzCore/QuartzCore.h>

@interface PHTextViewWithFrame ()

- (void)InitMembersForFrame;

@end

@implementation PHTextViewWithFrame

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    {
        // Initialization code
        [self InitMembersForFrame];
    }
    return self;
}

- (id)init
{
    self = [super init];
    {
        // Initialization code
        [self InitMembersForFrame];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        // Initialization code
        [self InitMembersForFrame];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self InitMembersForFrame];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    CGRect tmpRect = self.bounds;
    
    CGContextRef tmpContextRef = UIGraphicsGetCurrentContext();
    [self.LineColor set];
    CGContextSetLineWidth(tmpContextRef, self.LineWidth);
    CGContextAddRect(tmpContextRef, tmpRect);
    CGContextStrokePath(tmpContextRef);
}

#pragma - override methods

- (void)setLineWidth:(float)LineWidth
{
    _LineWidth = LineWidth;
    
    [self setNeedsDisplay];
}

- (void)setLineColor:(UIColor *)LineColor
{
    _LineColor = LineColor;
    
    [self setNeedsDisplay];
}

#pragma mark - Private Methods

- (void)InitMembersForFrame
{
    self.LineColor = [UIColor blackColor];
    self.LineWidth = 1;
}

@end
