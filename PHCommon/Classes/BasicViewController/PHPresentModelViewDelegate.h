//
//  PHPresentModelViewDelegate.h
//  PHCommon
//
//  Created by Ricol Wang on 16/09/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "utils.h"

typedef enum STYLE_PRESENT {LEFT_TO_CENTRE, RIGHT_TO_CENTRE, UP_TO_CENTRE, DOWN_TO_CENTRE, RANDOM_PRESENT}STYLE_PRESENT;
typedef enum STYLE_DISMISS {CENTRE_TO_LEFT, CENTRE_TO_RIGHT, CENTRE_TO_UP, CENTRE_TO_DOWN, RANDOM_DISMISS}STYLE_DISMISS;

@protocol PHPresentModelViewDelegate <NSObject>

- (void)AddTheModelView:(UIView *)view withStyle:(STYLE_PRESENT)style andComplete:(block)func;
- (void)RemoveTheModelView:(UIView *)view withStyle:(STYLE_DISMISS)style andComplete:(block)func;

@end
