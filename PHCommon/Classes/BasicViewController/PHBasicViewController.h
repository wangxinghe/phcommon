//
//  PHBasicViewController.h
//  PHCommon
//
//  Created by Ricol Wang on 16/09/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHPresentModelViewDelegate.h"

@interface PHBasicViewController : UIViewController <PHPresentModelViewDelegate>

{
    BOOL bAlreadyRun;
}

@property (strong, nonatomic) NSNumber *timeAnimate;

- (void)btnModalViewBackgroundOnTapped:(id)sender;

- (void)showLoading:(NSString *)message;
- (void)hideLoading;
- (instancetype)initWithDefault;

@end
