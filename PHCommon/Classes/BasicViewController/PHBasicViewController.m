//
//  PHBasicViewController.m
//  PHCommon
//
//  Created by Ricol Wang on 16/09/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import "PHBasicViewController.h"
#import "MBProgressHUD.h"

#define ANIMATION
#define TIME_ANIMATE 0.3

@interface PHBasicViewController () <MBProgressHUDDelegate>

@property (strong) UIView *viewCurrent;
@property (strong) UIButton *btnBackground;
@property (strong) MBProgressHUD *hud;
@property (strong) UIButton *btnBackgroundForHud;

@end

@implementation PHBasicViewController

- (instancetype)initWithDefault
{
    self = [super initWithNibName:NSStringFromClass(self.class) bundle:nil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

#pragma mark - Override Methods

- (NSNumber *)timeAnimate
{
    if (!_timeAnimate)
    {
        _timeAnimate = @(TIME_ANIMATE);
    }
    
    return _timeAnimate;
}

#pragma mark - PHPresentModelViewDelegate

//create a temp view to cover the parent view
- (void)AddTheModelView:(UIView *)view withStyle:(STYLE_PRESENT)style andComplete:(block)func
{
    if (!view)
        return;
    
    if (self.viewCurrent == view)
        return;
    
    if (self.viewCurrent && self.viewCurrent != view)
        return;
    
    if (self.view)
    {
        //add the view as a model view
        
        self.viewCurrent = view;
        self.btnBackground = [[UIButton alloc] initWithFrame:self.view.bounds];
        self.btnBackground.alpha = 0.0;
        self.btnBackground.backgroundColor = [UIColor blackColor];
        self.btnBackground.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self.btnBackground addTarget:self action:@selector(btnModalViewBackgroundOnTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.btnBackground];
        
        CGRect tmpRect = view.frame;
        tmpRect.origin = CGPointMake((self.view.bounds.size.width - tmpRect.size.width) / 2, (self.view.bounds.size.height - tmpRect.size.height) / 2);
        view.frame = tmpRect;
#ifdef ANIMATION
        
        CGRect tmpRect_Saved = view.frame;
        tmpRect = tmpRect_Saved;
        
        if (style == RANDOM_PRESENT)
            style = rand() % 4;
        
        switch (style)
        {
            case LEFT_TO_CENTRE:
                tmpRect.origin.x = tmpRect.size.width * (-1);
                break;
            case RIGHT_TO_CENTRE:
                tmpRect.origin.x = self.view.bounds.size.width;
                break;
            case UP_TO_CENTRE:
                tmpRect.origin.y = tmpRect.size.height * (-1);
                break;
            case DOWN_TO_CENTRE:
                tmpRect.origin.y = self.view.bounds.size.height;
                break;
            default:
                tmpRect.origin.x = tmpRect.size.width * (-1);
                break;
        }
        
        view.frame = tmpRect;
        [self.view addSubview:view];
        
        [UIView animateWithDuration:self.timeAnimate.floatValue animations:^(){
            view.frame = tmpRect_Saved;
            self.btnBackground.alpha = 0.3;
        } completion:^(BOOL finished){
            if (func) func();
        }];
#else
        [self.view addSubview:view];
        self.btnBackground.alpha = 0.3;
        if (func) func();
#endif
    }
}

- (void)RemoveTheModelView:(UIView *)view withStyle:(STYLE_DISMISS)style andComplete:(block)func
{
    if (!view)
        return;
    
    if (!self.btnBackground)
        return;
    
    if (!self.view)
        return;
    
    if (!self.viewCurrent)
        return;
    
    if (self.viewCurrent != view)
        return;
    
#ifdef ANIMATION
    CGRect tmpRect = view.frame;
    
    if (style == RANDOM_DISMISS)
        style = rand() % 4;
    
    switch (style)
    {
        case CENTRE_TO_LEFT:
            tmpRect.origin.x = tmpRect.size.width * (-1);
            break;
        case CENTRE_TO_RIGHT:
            tmpRect.origin.x = self.view.bounds.size.width;
            break;
        case CENTRE_TO_UP:
            tmpRect.origin.y = tmpRect.size.height * (-1);
            break;
        case CENTRE_TO_DOWN:
            tmpRect.origin.y = self.view.bounds.size.height;
            break;
        default:
            tmpRect.origin.x = tmpRect.size.width * (-1);
            break;
    }
    
    [UIView animateWithDuration:self.timeAnimate.floatValue animations:^(){
        view.frame = tmpRect;
        self.btnBackground.alpha = 0;
    } completion:^(BOOL finished){
        [view removeFromSuperview];
        [self.btnBackground removeFromSuperview];
        self.btnBackground = nil;
        self.viewCurrent = nil;
        
        if (func) func();
    }];
#else
    [view removeFromSuperview];
    [self.btnBackground removeFromSuperview];
    self.btnBackground = nil;
    self.viewCurrent = nil;
    
    if (func) func();
#endif
}

- (void)btnModalViewBackgroundOnTapped:(id)sender
{
    [self RemoveTheModelView:self.viewCurrent withStyle:CENTRE_TO_DOWN andComplete:nil];
}

#pragma mark - MBProgressHUDDelegate

- (void)hudWasHidden:(MBProgressHUD *)hud
{
    [hud removeFromSuperview];
    hud = nil;
}

#pragma mark - Public Methods

- (void)showLoading:(NSString *)message
{
    [self hideLoading];
    
    [self.hud hide:NO];
    
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
    
    self.hud = [MBProgressHUD showHUDAddedTo:mainWindow.rootViewController.view animated:YES];
    self.hud.labelText = message;
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.delegate = self;
    
    [self.btnBackgroundForHud removeFromSuperview];
    self.btnBackgroundForHud = nil;
    
    UIView *tmpView = mainWindow.rootViewController.view;
    self.btnBackgroundForHud = [[UIButton alloc] initWithFrame:tmpView.bounds];
    self.btnBackgroundForHud.backgroundColor = [UIColor blackColor];
    self.btnBackgroundForHud.alpha = 0;
    [tmpView addSubview:self.btnBackgroundForHud];
    mainWindow.rootViewController.view.userInteractionEnabled = NO;
}

- (void)hideLoading
{
    [self.hud hide:YES];
    
    [self.btnBackgroundForHud removeFromSuperview];
    self.btnBackgroundForHud = nil;
    
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
    mainWindow.rootViewController.view.userInteractionEnabled = YES;
}


@end
