//
//  PHActivityIndicatorWithColor.m
//  PHCommon
//
//  Created by Ricol Wang on 21/11/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import "PHActivityIndicatorWithColor.h"
#import "utils.h"

typedef enum DIRECTION
{INCREASE, DECREASE}
DIRECTION;

typedef enum NUM
{NUMRED, NUMGREEN, NUMBLUE}
NUM;

@interface PHActivityIndicatorWithColor ()

{
    int red, green, blue;
    BOOL bStop;
    NUM num;
    DIRECTION directionRed;
    DIRECTION directionGreen;
    DIRECTION directionBlue;
}

@end

@implementation PHActivityIndicatorWithColor

#pragma mark - Life Cycle

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self initColor];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initColor];
    }
    return self;
}

- (instancetype)initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyle)style
{
    self = [super initWithActivityIndicatorStyle:style];
    if (self)
    {
        [self initColor];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initColor];
    }
    return self;
}

#pragma mark - Override Methods

- (void)startAnimating
{
    if (self.isAnimating) return;
    [super startAnimating];
    bStop = NO;
    
    //animate color changing
    if (!self.bColorStopChange)
    {
        [utils runInBackgroundThread:^(){
            [self changeColor];
        }];
    }
}

- (void)stopAnimating
{
    [super stopAnimating];
    
    bStop = YES;
}

- (void)dealloc
{
    [self stopAnimating];
    bStop = YES;
}

#pragma mark - Private Methods

- (void)initColor
{
    red = 255;
    green = 0;
    blue = 0;
    num = NUMRED;
    directionRed = DECREASE;
    directionGreen = INCREASE;
    directionBlue = INCREASE;
    self.delay = 0.05;
    self.colorStep = 5;
}

- (void)changeColor
{
    while (!bStop)
    {
        [NSThread sleepForTimeInterval:self.delay];
        
        //update RGB
        if (num == NUMRED)
        {
            if (directionRed == INCREASE)
            {
                red += self.colorStep;
                if (red >= 255)
                {
                    red = 255;
                    directionRed = DECREASE;
                    num = NUMBLUE;
                }
            }else
            {
                red -= self.colorStep;
                if (red <= 0)
                {
                    red = 0;
                    directionRed = INCREASE;
                    num = NUMBLUE;
                }
            }
        }else if (num == NUMGREEN)
        {
            if (directionGreen == INCREASE)
            {
                green += self.colorStep;
                if (green >= 255)
                {
                    green = 255;
                    directionGreen = DECREASE;
                    num = NUMRED;
                }
            }else
            {
                green -= self.colorStep;
                if (green <= 0)
                {
                    green = 0;
                    directionGreen = INCREASE;
                    num = NUMRED;
                }
            }
        }else
        {
            if (directionBlue == INCREASE)
            {
                blue += self.colorStep;
                if (blue >= 255)
                {
                    blue = 255;
                    directionBlue = DECREASE;
                    num = NUMGREEN;
                }
            }else
            {
                blue -= self.colorStep;
                if (blue <= 0)
                {
                    blue = 0;
                    directionBlue = INCREASE;
                    num = NUMGREEN;
                }
            }
        }
        
        //change color
        [utils runInMainThread:^(){
            self.color = [UIColor colorWithRed:(red * 1.0) / 255.0 green:(green * 1.0) / 255.0 blue:(blue * 1.0) / 255.0 alpha:1];
        }];
    }
}

@end
