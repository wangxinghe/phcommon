//
//  PHActivityIndicatorWithColor.h
//  PHCommon
//
//  Created by Ricol Wang on 21/11/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHActivityIndicatorWithColor : UIActivityIndicatorView

@property float delay;
@property BOOL bColorStopChange;
@property int colorStep;

@end
