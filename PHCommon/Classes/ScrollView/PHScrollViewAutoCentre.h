//
//  PHScrollViewAutoCentre.h
//  WalkCover
//
//  Created by Ricol Wang on 10/11/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHScrollViewAutoCentre : UIScrollView

@property (strong) UIView *viewContent;
@property BOOL animate;
@property float animationTime;

@end
