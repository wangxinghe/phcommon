//
//  PHScrollViewAutoCentre.m
//  WalkCover
//
//  Created by Ricol Wang on 10/11/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHScrollViewAutoCentre.h"

@implementation PHScrollViewAutoCentre

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // center the image as it becomes smaller than the size of the screen
    CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = self.viewContent.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;
    
    if (self.animate)
    {
        [UIView animateWithDuration:self.animationTime animations:^(){
            self.viewContent.frame = frameToCenter;
        }];
    }else
        self.viewContent.frame = frameToCenter;
}

@end
