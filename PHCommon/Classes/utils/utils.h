//
//  utils.h
//  PHCustomControls
//
//  Created by Ricol Wang on 18/09/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#define DOCUMENTS_DIRECTORY [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
#define LIBRARY_DIRECTORY [NSHomeDirectory() stringByAppendingPathComponent:@"Library"]

typedef void(^block)();

@class AppDelegate;

@interface utils : NSObject

//string related
+ (NSString *)trimLeft:(NSString *)string;
+ (NSString *)trimRight:(NSString *)string;
+ (NSString *)trimBoth:(NSString *)string;
+ (NSString *)getStringWithFixedLength:(NSString *)string andLength:(int)length;
+ (NSString *)getFormattedString:(NSString *)string andSeperator:(NSString *)seperator;

//identifier related
+ (NSString *)getUniqueTimeStamp;
+ (NSString *)getUniqueNumericString:(int)length;
+ (NSString *)getUniqueAlphaNumericString:(int)length;

//date related
+ (NSDate *)begginingOfDay:(NSDate *)date;
+ (NSDate *)endOfDay:(NSDate *)date;
+ (NSString *)weekNameForDay:(NSDate *)date;
+ (NSDate *)firstDayOfNextWeek:(NSDate *)dateForNow;
+ (NSDate *)lastDayOfNextWeek:(NSDate *)dateForNow;
+ (NSDate *)firstDayOfTheMonth:(NSDate *)date;
+ (NSDate *)lastDayOfTheMonth:(NSDate *)date;

//date formatter related
+ (NSDateFormatter *)getDateFormatterForServer;
+ (NSDateFormatter *)getDateFormatterForServerInUTC;
+ (NSDateFormatter *)getDateFormatterInUTC;

//location related
+ (NSArray *)getAddressFromCoordinate2D:(float)lat andLong:(float)lon andReady:(bool *)ready;
+ (CLLocationCoordinate2D)getLocationFromAddressString:(NSString *)addressStr;

//concurrent related
+ (void)runAfter:(float)seconds andBlock:(block)myFunction;
+ (void)runInBackgroundThread:(block)func;
+ (void)runInMainThread:(block)func;

//general
+ (NSString *)getAppVersion;
+ (AppDelegate *)getTheAppDelegate;
+ (UIColor *)randomColor;

//system related
+ (BOOL)isIPhone;
+ (BOOL)isIOS7;
+ (BOOL)isIOS8;

+ (CGRect)getWindowRect;

//file/bundle related
+ (NSArray *)getAllFilesUnderDirectory:(NSString *)directory andSubDirectory:(BOOL)subDirectory;
+ (NSString *)getFileNameWithoutExtension:(NSString *)filename;
+ (NSString *)getFileExtension:(NSString *)file;
+ (NSBundle *)getBundleInDocument:(NSString *)bundleName;
+ (NSBundle *)getBundleInApp:(NSString *)bundleName inDirectory:(NSString *)directory;

//preview
+ (UIImage *)previewFromFileAtPath:(NSString*)path ratio:(CGFloat)ratio withError:(NSError **)error;
+ (UIImage *)getImageForPDF:(NSString *)finalPath andPdfThumbSize:(CGSize)pdfThumbSize;

@end
