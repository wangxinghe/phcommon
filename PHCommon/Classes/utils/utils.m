//
//  utils.m
//  PHCustomControls
//
//  Created by Ricol Wang on 18/09/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import "utils.h"
#import <SBJson/SBJson.h>
#import <AVFoundation/AVFoundation.h>

@implementation utils

#pragma mark - Static Methods

/**
 Returns the given date with the hours and mins set to 00:00:00
 */
+ (NSDate *)begginingOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:date];
    
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    
    return [cal dateFromComponents:components];
}

/**
 Returns the given date with the hours, mins and seconds set to 23:59:59
 */
+ (NSDate *)endOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:date];
    
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    
    return [cal dateFromComponents:components];
}

+ (NSString *)getUniqueTimeStamp
{
    return [[NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]] stringValue];
}

+ (NSString *)getUniqueNumericString:(int)length
{
    NSMutableArray *tmpMutableArray = [NSMutableArray new];
    
    for (char c = '0'; c <= '9'; c++)
    {
        NSString *tmpStr = [NSString stringWithFormat:@"%c", c];
        [tmpMutableArray addObject:tmpStr];
    }
    
    NSMutableString *tmpMutableString = [[NSMutableString alloc] init];
    
    for (int i = 0; i < length; i++)
    {
        int num = arc4random() % tmpMutableArray.count;
        NSString *tmpStr = tmpMutableArray[num];
        [tmpMutableString appendString:tmpStr];
    }
    
    return tmpMutableString;
}

/*
 delete all empty space at the front of the string
 */
+ (NSString *)trimLeft:(NSString *)string
{
    if (!string) return string;
    
    int len = string.length;
    
    int i;
    for (i = 0; i < len; i++)
        if ([string characterAtIndex:i] != ' ') break;
    
    if (i < len && i >= 0)
        return [string substringFromIndex:i];
    else
        return [string stringByReplacingOccurrencesOfString:@" " withString:@""];
}

/*
 delete all empty space at the end of the string
 */
+ (NSString *)trimRight:(NSString *)string
{
    if (!string) return string;
    
    int len = string.length;
    
    int i;
    for (i = len - 1; i >= 0; i--)
        if ([string characterAtIndex:i] != ' ') break;
    
    if (i < len && i >= 0)
        return [string substringToIndex:i + 1];
    else
        return [string stringByReplacingOccurrencesOfString:@" " withString:@""];
}

/*
 delete all empty space at the front and end of the string
 */
+ (NSString *)trimBoth:(NSString *)string
{
    NSString *tmpStr = [utils trimLeft:string];
    tmpStr = [utils trimRight:tmpStr];
    
    return tmpStr;
}

+ (NSBundle *)getBundleInDocument:(NSString *)bundleName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *tmpBundleName = [NSString stringWithFormat:@"%@.bundle", bundleName];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:tmpBundleName];
    NSBundle *bundle = [NSBundle bundleWithPath:filePath];
    
    return bundle;
}

+ (NSBundle *)getBundleInApp:(NSString *)bundleName inDirectory:(NSString *)directory
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:bundleName ofType:@"bundle" inDirectory:directory];
    NSBundle *bundle = [NSBundle bundleWithPath:filePath];
    
    return bundle;
}

+ (UIColor *)randomColor
{
    double red = (rand() % 256) / 255.0;
    double green = (rand() % 256) / 255.0;
    double blue = (rand() % 256) / 255.0;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:1];
}

+ (BOOL)isIPhone
{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone;
}

+ (NSString *)getFileExtension:(NSString *)file
{
    if (file)
    {
        NSArray *tmpArray = [file componentsSeparatedByString:@"."];
        if (tmpArray.count > 1)
            return tmpArray.lastObject;
    }
    return nil;
}

+ (NSString *)getFileNameWithoutExtension:(NSString *)filename
{
    NSString *tmpFileName = [filename copy];
    NSArray *tmpArray = [tmpFileName componentsSeparatedByString:@"."];
    
    if (tmpArray.count > 1)
    {
        NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] initWithArray:tmpArray];
        [tmpMutableArray removeLastObject];
        NSString *tmpStr_Result = [[tmpMutableArray componentsJoinedByString:@"."] copy];
        return tmpStr_Result;
    }
    
    return filename;
}

+ (NSArray *)getAllFilesUnderDirectory:(NSString *)directory andSubDirectory:(BOOL)subDirectory
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] init];
    [tmpMutableArray removeAllObjects];
    
    NSArray *tmpArray_AllFileNames = [fm contentsOfDirectoryAtPath:directory error:&error];
    
    if (error)
    {
        NSLog(@"Error: %@", error);
        return nil;
    }else
    {
        for (NSString *tmpFileName in tmpArray_AllFileNames)
        {
            NSString *tmpFileFullPath = [NSString stringWithFormat:@"%@/%@", directory, tmpFileName];
            BOOL bIsDirectory = NO;
            if ([fm fileExistsAtPath:tmpFileFullPath isDirectory:&bIsDirectory])
            {
                if (!bIsDirectory)
                    [tmpMutableArray addObject:tmpFileFullPath];
                else
                {
                    if (subDirectory)
                    {
                        NSArray *tmpArray_AllFilesForSubDirectory = [utils getAllFilesUnderDirectory:tmpFileFullPath andSubDirectory:subDirectory];
                        [tmpMutableArray addObjectsFromArray:tmpArray_AllFilesForSubDirectory];
                    }
                }
            }
        }
    }
    
    return tmpMutableArray;
}

+ (NSString *)weekNameForDay:(NSDate *)date
{
    NSCalendar *tmpGregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *tmpWeekComponents = [tmpGregorian components:NSWeekdayCalendarUnit fromDate:date];
    NSString *tmpStr = @"UNKNOWN";
    
    switch ([tmpWeekComponents weekday])
    {
        case 1:
            tmpStr = @"SUNDAY";
            break;
        case 2:
            tmpStr = @"MONDAY";
            break;
        case 3:
            tmpStr = @"TUESDAY";
            break;
        case 4:
            tmpStr = @"WEDNESDAY";
            break;
        case 5:
            tmpStr = @"THURSDAY";
            break;
        case 6:
            tmpStr = @"FRIDAY";
            break;
        case 7:
            tmpStr = @"SATURDAY";
            break;
        default:
            break;
    }
    
    return tmpStr;
}

+ (NSDate *)firstDayOfNextWeek:(NSDate *)dateForNow
{
    NSDate *tmpDate_Result = nil;
    NSDate *tmpDate_Now = dateForNow;
    NSDate *tmpDate_NextDay;
    NSCalendar *tmpGregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *tmpWeekComponents = [tmpGregorian components:NSWeekdayCalendarUnit fromDate:tmpDate_Now];
    NSDateComponents *tmpWeekOfYearComponents = [tmpGregorian components:NSWeekOfYearCalendarUnit fromDate:tmpDate_Now];
    int weekOfYearForToday = [tmpWeekOfYearComponents weekOfYear];
    
    //find the start date of next week
    int i = 1;
    BOOL bFound = false;
    
    do
    {
        tmpDate_NextDay = [NSDate dateWithTimeInterval:(i * 24 * 60 * 60) sinceDate:tmpDate_Now];
        tmpWeekComponents = [tmpGregorian components:NSWeekdayCalendarUnit fromDate:tmpDate_NextDay];
        int weekday = [tmpWeekComponents weekday];
        tmpWeekOfYearComponents = [tmpGregorian components:NSWeekOfYearCalendarUnit fromDate:tmpDate_NextDay];
        int weekOfYear = [tmpWeekOfYearComponents weekOfYear];
        if ((weekday == 1) && (weekOfYear != weekOfYearForToday))
        {
            tmpDate_Result = tmpDate_NextDay;
            bFound = true;
        }else
            ;
        i++;
    } while (!bFound);
    
    return tmpDate_Result;
}

+ (NSDate *)lastDayOfNextWeek:(NSDate *)dateForNow
{
    NSDate *tmpDate_Result = nil;
    NSDate *tmpDate_Now = dateForNow;
    NSDate *tmpDate_NextDay;
    NSCalendar *tmpGregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *tmpWeekComponents = [tmpGregorian components:NSWeekdayCalendarUnit fromDate:tmpDate_Now];
    NSDateComponents *tmpWeekOfYearComponents = [tmpGregorian components:NSWeekOfYearCalendarUnit fromDate:tmpDate_Now];
    int weekOfYearForToday = [tmpWeekOfYearComponents weekOfYear];
    
    //find the last date of next week
    int i = 1;
    BOOL bFound = false;
    
    do
    {
        tmpDate_NextDay = [NSDate dateWithTimeInterval:(i * 24 * 60 * 60) sinceDate:tmpDate_Now];
        tmpWeekComponents = [tmpGregorian components:NSWeekdayCalendarUnit fromDate:tmpDate_NextDay];
        int weekday = [tmpWeekComponents weekday];
        tmpWeekOfYearComponents = [tmpGregorian components:NSWeekOfYearCalendarUnit fromDate:tmpDate_NextDay];
        int weekOfYear = [tmpWeekOfYearComponents weekOfYear];
        if ((weekday == 7) && (weekOfYear != weekOfYearForToday))
        {
            tmpDate_Result = tmpDate_NextDay;
            bFound = true;
        }else
            ;
        i++;
    } while (!bFound);
    
    return tmpDate_Result;
}

+ (NSString *)getStringWithFixedLength:(NSString *)string andLength:(int)length
{
    NSMutableString *tmpMutableString = [[NSMutableString alloc] initWithString:@""];
    
    if (tmpMutableString.length <= length)
    {
        int numberOfSpaces = length - tmpMutableString.length;
        for (int i = 1; i <= numberOfSpaces; i++)
        {
            [tmpMutableString appendString:string];
        }
    }
    
    return tmpMutableString;
}

+ (BOOL)isIOS7
{
    return floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1;
}

+ (BOOL)isIOS8
{
    return floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1;
}

+ (CGRect)getWindowRect
{
    CGRect tmpRect = CGRectZero;
    UIWindow *keyWindow = [[[UIApplication sharedApplication] delegate] window];
    tmpRect = keyWindow.rootViewController.view.bounds;
//    NSLog(@"getWindowRect: %f, %f, %f, %f", tmpRect.origin.x, tmpRect.origin.y, tmpRect.size.width, tmpRect.size.height);
    return tmpRect;
}

+ (NSDate *)firstDayOfTheMonth:(NSDate *)date
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:date];
    components.day = 1;
    NSDate *dayOneInCurrentMonth = [gregorian dateFromComponents:components];
    return dayOneInCurrentMonth;
}

+ (NSDate *)lastDayOfTheMonth:(NSDate *)date
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* comps = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:date];
    [comps setMonth:[comps month]+1];
    [comps setDay:0];
    NSDate *tDateMonth = [calendar dateFromComponents:comps];
    return tDateMonth;
}

+ (NSString *)getUniqueAlphaNumericString:(int)length
{
    NSMutableArray *tmpMutableArray = [NSMutableArray new];
    
    for (char c = '0'; c <= '9'; c++)
    {
        NSString *tmpStr = [NSString stringWithFormat:@"%c", c];
        [tmpMutableArray addObject:tmpStr];
    }
    
    for (char c = 'a'; c <= 'z'; c++)
    {
        NSString *tmpStr = [NSString stringWithFormat:@"%c", c];
        [tmpMutableArray addObject:tmpStr];
    }
    
    for (char c = 'A'; c <= 'Z'; c++)
    {
        NSString *tmpStr = [NSString stringWithFormat:@"%c", c];
        [tmpMutableArray addObject:tmpStr];
    }
    
    NSMutableString *tmpMutableString = [[NSMutableString alloc] init];
    
    for (int i = 0; i < length; i++)
    {
        int num = arc4random() % tmpMutableArray.count;
        NSString *tmpStr = tmpMutableArray[num];
        [tmpMutableString appendString:tmpStr];
    }
    
    return tmpMutableString;
}

+ (NSString *)getFormattedString:(NSString *)string andSeperator:(NSString *)seperator
{
    if (!string) return @"";
    
    NSArray *tmpArray = [string componentsSeparatedByString:seperator];
    NSMutableArray *tmpMutableArray = [NSMutableArray new];
    
    for (uint i = 0; i < tmpArray.count; i++)
    {
        NSString *tmpStr = tmpArray[i];
        NSString *tmpStrNew = [utils trimBoth:tmpStr];
        if ([tmpStrNew isEqualToString:@""]) continue;
        [tmpMutableArray addObject:tmpStrNew];
    }
    
    NSString *tmpStrResult = [tmpMutableArray componentsJoinedByString:seperator];
    return tmpStrResult;
}

+ (void)runAfter:(float)seconds andBlock:(block)myFunction
{
    if (myFunction)
    {
        double delayInSeconds = seconds;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            if (myFunction)
                myFunction();
        });
    }
}

+ (NSDateFormatter *)getDateFormatterInUTC
{
    NSDateFormatter *tmpDateFormatter = [[NSDateFormatter alloc] init];
    tmpDateFormatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    return tmpDateFormatter;
}

+ (NSDateFormatter *)getDateFormatterForServer
{
    NSDateFormatter *tmpDateFormatter = [NSDateFormatter new];
    tmpDateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    return tmpDateFormatter;
}

+ (NSDateFormatter *)getDateFormatterForServerInUTC
{
    NSDateFormatter *tmpDateFormatter = [utils getDateFormatterForServer];
    tmpDateFormatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    return tmpDateFormatter;
}

+ (NSArray *)getAddressFromCoordinate2D:(float)lat andLong:(float)lon andReady:(bool *)ready
{
    NSDictionary * dict = [utils getGeoCodeDetails:lat lon:lon];
    NSArray *keys = [dict allValues];
    NSMutableArray * addressArr = [[NSMutableArray alloc] initWithObjects:@"", @"", @"", @"", @"", @"", nil];
    *ready = NO;
    
    if ([keys[([keys count]-1)] isEqualToString:@"OK"])
    {
        NSDictionary * newDict = keys[0][0];
        
        if (newDict[@"address_components"] != nil)
        {
            for (NSUInteger i = 0; i<[((NSArray *)newDict[@"address_components"]) count]; i++)
            {
                if ([newDict[@"address_components"][i][@"types"][0] rangeOfString:@"street_number"].location != NSNotFound)
                {
                    addressArr[0] = newDict[@"address_components"][i][@"long_name"];
                }else if ([newDict[@"address_components"][i][@"types"][0] rangeOfString:@"route"].location != NSNotFound)
                {
                    addressArr[1] = newDict[@"address_components"][i][@"long_name"];
                }else if ([newDict[@"address_components"][i][@"types"][0] rangeOfString:@"locality"].location != NSNotFound)
                {
                    addressArr[2] = newDict[@"address_components"][i][@"long_name"];
                }else if ([newDict[@"address_components"][i][@"types"][0] rangeOfString:@"administrative"].location != NSNotFound)
                {
                    addressArr[3] = newDict[@"address_components"][i][@"short_name"];
                }else if ([newDict[@"address_components"][i][@"types"][0] rangeOfString:@"postal"].location != NSNotFound)
                {
                    addressArr[4] = newDict[@"address_components"][i][@"long_name"];
                }else if ([newDict[@"address_components"][i][@"types"][0] rangeOfString:@"country"].location != NSNotFound)
                {
                    addressArr[5] = newDict[@"address_components"][i][@"long_name"];
                }
            }
            
            *ready = YES;
        }
    }
    
    return addressArr;
}

+ (NSDictionary *)getGeoCodeDetails:(float)lat lon:(float)lon
{
    NSString * tempURL = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true", lat, lon];
    
    NSString *jsonString = [utils parseFileToStrAtURL:tempURL];
    
    NSData *newJsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    SBJsonParser *json = [SBJsonParser new];
    
    return [json objectWithData:newJsonData];
	
}

+ (NSString *)parseFileToStrAtURL:(NSString *)URL
{
	NSString *error = @"error";
	
	NSError *err = [NSError new];
	
	NSString *url = [URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSString *stringFile = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:&err];
	
	
	if(err.code != 0)
    {
		return error;
	}
	
	return stringFile;
}

+ (CLLocationCoordinate2D)getLocationFromAddressString:(NSString *)addressStr
{
    CLLocationCoordinate2D location;
    double lat = 0.0;
    double lon = 0.0;
    
    location.latitude = lat;
    location.longitude = lon;
    
    @try
    {
        NSString *urlStr = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?&address=%@&sensor=true",[addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSError *error;
        
        NSMutableDictionary *jsonData = [[NSString stringWithContentsOfURL:[NSURL URLWithString:urlStr] encoding:NSUTF8StringEncoding error:&error] JSONValue];
        
        NSMutableArray *value = [jsonData objectForKey:@"results"];
        
        if([value count] > 0)
        {
            location.latitude = [[[[value[0] objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] doubleValue];
            location.longitude = [[[[value[0] objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] doubleValue];
            
            return location;
        }
        else
        {
            location.latitude  = lat;
            location.longitude = lon;
            
        }
    }
    @catch (NSException *exception)
    {
        location.latitude  = lat;
        location.longitude = lon;
    }
    @finally
    {
        return location;
    }
}

+ (void)runInBackgroundThread:(block)func
{
    static dispatch_queue_t queue = nil;
    
    if (func)
    {
        if ([NSThread isMainThread])
        {
            if (!queue) queue = dispatch_queue_create("au.philology.MySerialQueue", nil);
            dispatch_async(queue, func);
        }
        else
            func();
    }
}

+ (void)runInMainThread:(block)func
{
    if (func)
    {
        if ([NSThread isMainThread])
            func();
        else
            dispatch_async(dispatch_get_main_queue(), func);
    }
}

+ (NSString *)getAppVersion
{
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
}

+ (AppDelegate *)getTheAppDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

+ (UIImage*)previewFromFileAtPath:(NSString*)path ratio:(CGFloat)ratio withError:(NSError **)error
{
    UIImage *thumbnail = nil;
    
    AVAsset *asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:path]];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime duration = asset.duration;
    CGFloat durationInSeconds = duration.value / duration.timescale;
    CMTime time = CMTimeMakeWithSeconds(durationInSeconds * ratio, (int)duration.value);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:error];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return thumbnail;
}

+ (UIImage *) getImageForPDF:(NSString *)finalPath andPdfThumbSize:(CGSize)pdfThumbSize
{
    NSURL* pdfFileUrl = [NSURL fileURLWithPath:finalPath];
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((__bridge CFURLRef)pdfFileUrl);
    CGPDFPageRef page;
    
    CGRect aRect = CGRectMake(0, 0, pdfThumbSize.width, pdfThumbSize.height); // thumbnail size
    
    UIGraphicsBeginImageContext(aRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIImage* thumbnailImage = nil;
    
    
    NSUInteger totalNum = CGPDFDocumentGetNumberOfPages(pdf);
    
    if (totalNum > 0)
    {
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, 0.0, aRect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        
        CGContextSetGrayFillColor(context, 1.0, 1.0);
        CGContextFillRect(context, aRect);
        
        
        // Grab the first PDF page
        page = CGPDFDocumentGetPage(pdf, 1);
        
        CGAffineTransform pdfTransform = CGPDFPageGetDrawingTransform(page, kCGPDFMediaBox, aRect, 0, true);
        // And apply the transform.
        CGContextConcatCTM(context, pdfTransform);
        
        CGContextDrawPDFPage(context, page);
        
        // Create the new UIImage from the context
        thumbnailImage = UIGraphicsGetImageFromCurrentImageContext();
        
        //Use thumbnailImage (e.g. drawing, saving it to a file, etc)
        CGContextRestoreGState(context);
    }
    
    UIGraphicsEndImageContext();
    CGPDFDocumentRelease(pdf);
    
    return thumbnailImage;
}

@end
