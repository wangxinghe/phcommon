//
//  TreeTableViewCellDelegate.h
//  TreeMenu_Demo
//
//  Created by Ricol Wang on 2/11/13.
//  Copyright (c) 2013 ricol wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TreeTableViewCell, TreeNode;

@protocol TreeTableViewCellDelegate <NSObject>

@optional

- (void)TreeTableViewCell:(TreeTableViewCell *)cell didTapIconWithNode:(TreeNode *)Node;
- (void)TreeTableViewCell:(TreeTableViewCell *)cell didSwipeToLeftWithNode:(TreeNode *)Node;
- (void)TreeTableViewCell:(TreeTableViewCell *)cell didSwipeToRightWithNode:(TreeNode *)Node;

@end
