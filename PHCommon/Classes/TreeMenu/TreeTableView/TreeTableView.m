//
//  TreeTableView.m
//  TreeMenu_Demo
//
//  Created by Ricol Wang on 2/11/13.
//  Copyright (c) 2013 ricol wang. All rights reserved.
//

#import "TreeTableView.h"
#import "TreeTableViewCell.h"

#define INCLUDE_ROOT NO
#define HEIGHT_CELL_DEFAULT 40

@interface TreeTableView () <UITableViewDataSource, UITableViewDelegate, TreeTableViewCellDelegate>

@property NSMutableArray *arrayMenus;
@property Tree *tree;
@property BOOL bInOperation;

- (void)InitMember;
- (TreeTableViewCell *)getCellByNode:(TreeNode *)theTreeNode;

@end

@implementation TreeTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        [self InitMember];
    }
    return self;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self InitMember];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self InitMember];
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self InitMember];
}

#pragma mark - Override Methods

- (void)reloadData
{
    [self RefreshModel];
    
    [super reloadData];
}

- (void)setDelegateForTheTreeTableView:(id<TreeTableViewDelegate>)delegateForTheTreeTableView
{
    _delegateForTheTreeTableView = delegateForTheTreeTableView;
    
    self.tree = [_delegateForTheTreeTableView TreeForTheTreeTableView:self];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TreeNode *tmpNode = self.arrayMenus[indexPath.row];
    
    TreeTableViewCell *tmpCell = tmpNode.cell;
    if (!tmpCell)
    {
        tmpCell = [self.delegateForTheTreeTableView cellForTreeTableView:self andTreeNode:tmpNode];
        tmpCell.node = tmpNode;
        tmpCell.delegate = self;
        
        tmpNode.cell = tmpCell;
    }
    
    [tmpCell updateGUI];
    
    return tmpCell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMenus.count;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TreeNode *tmpNode = self.arrayMenus[indexPath.row];
    
    TreeTableViewCell *tmpCell = tmpNode.cell;
    if (!tmpCell)
    {
        tmpCell = [self.delegateForTheTreeTableView cellForTreeTableView:self andTreeNode:tmpNode];
        tmpCell.node = tmpNode;
        tmpCell.delegate = self;
        
        tmpNode.cell = tmpCell;
    }
    
    return tmpCell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    if ([self.delegateForTheTreeTableView respondsToSelector:@selector(TreeTableView:moveRowAtIndexPath:toIndexPath:)])
        [self.delegateForTheTreeTableView TreeTableView:self moveRowAtIndexPath:sourceIndexPath toIndexPath:destinationIndexPath];
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegateForTheTreeTableView respondsToSelector:@selector(TreeTableView:shouldIndentWhileEditingRowAtIndexPath:)])
        return [self.delegateForTheTreeTableView TreeTableView:self shouldIndentWhileEditingRowAtIndexPath:indexPath];
    else
        return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegateForTheTreeTableView respondsToSelector:@selector(TreeTableView:editingStyleForRowAtIndexPath:)])
        return [self.delegateForTheTreeTableView TreeTableView:self editingStyleForRowAtIndexPath:indexPath];
    else
        return UITableViewCellEditingStyleDelete;
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    if ([self.delegateForTheTreeTableView respondsToSelector:@selector(TreeTableView:targetIndexPathForMoveFromRowAtIndexPath:toProposedIndexPath:)])
        return [self.delegateForTheTreeTableView TreeTableView:self targetIndexPathForMoveFromRowAtIndexPath:sourceIndexPath toProposedIndexPath:proposedDestinationIndexPath];
    else
        return sourceIndexPath;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegateForTheTreeTableView respondsToSelector:@selector(TreeTableView:canMoveRowAtIndexPath:)])
        return [self.delegateForTheTreeTableView TreeTableView:self canMoveRowAtIndexPath:indexPath];
    else
        return YES;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegateForTheTreeTableView respondsToSelector:@selector(TreeTableView:canEditRowAtIndexPath:)])
        return [self.delegateForTheTreeTableView TreeTableView:self canEditRowAtIndexPath:indexPath];
    else
        return YES;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.editing) return;
    
    TreeTableViewCell *tmpCell = (TreeTableViewCell *)[self cellForRowAtIndexPath:indexPath];
    TreeNode *tmpNode = tmpCell.node;
    
    if (tmpNode.bSeperator) return;
    
    if ([self.delegateForTheTreeTableView respondsToSelector:@selector(TreeTableView:didSelectRowAtIndexPath:)])
        [self.delegateForTheTreeTableView TreeTableView:self didSelectRowAtIndexPath:indexPath];
}

#pragma mark - TreeTableViewCellDelegate

- (void)TreeTableViewCell:(TreeTableViewCell *)cell didTapIconWithNode:(TreeNode *)Node
{
    if (self.editing) return;
    
    if ([self.delegateForTheTreeTableView respondsToSelector:@selector(TreeTableView:didTapIconWithNode:)])
        [self.delegateForTheTreeTableView TreeTableView:self didTapIconWithNode:Node];
}

- (void)TreeTableViewCell:(TreeTableViewCell *)cell didSwipeToLeftWithNode:(TreeNode *)Node
{
    if ([self.delegateForTheTreeTableView respondsToSelector:@selector(TreeTableView:didSwipeToLeftWithNode:)])
        [self.delegateForTheTreeTableView TreeTableView:self didSwipeToLeftWithNode:Node];
}

- (void)TreeTableViewCell:(TreeTableViewCell *)cell didSwipeToRightWithNode:(TreeNode *)Node
{
    if (self.editing) return;
    
    if ([self.delegateForTheTreeTableView respondsToSelector:@selector(TreeTableView:didSwipeToRightWithNode:)])
        [self.delegateForTheTreeTableView TreeTableView:self didSwipeToRightWithNode:Node];
}

#pragma mark - Private Methods

- (void)InitMember
{
    self.arrayMenus = [NSMutableArray new];
    
    self.dataSource = self;
    self.delegate = self;
}

- (TreeTableViewCell *)getCellByNode:(TreeNode *)theTreeNode
{
    for (TreeTableViewCell *tmpCell in self.visibleCells)
    {
        if (tmpCell.node == theTreeNode) return tmpCell;
    }
    
    return nil;
}

#pragma mark - Public Methods

- (void)RefreshModel
{
    NSArray *tmpArray = [self.tree getAllUnfoldedTreeNodeUnderTreeNode:self.tree.root andItself:INCLUDE_ROOT];
    
    [self.arrayMenus removeAllObjects];
    [self.arrayMenus addObjectsFromArray:tmpArray];
}

- (BOOL)RemoveNode:(TreeNode *)node
{
    if (!node) return NO;
    
    //only support delete a leaf. for deleting a branch, if the cell is not visible, the app will crash;
    if (![node isLeaf]) return NO;
    
    if (node == self.tree.root) return NO;
    
    TreeNode *tmpNodeParent = node.parentTreeNode;
    
    if ([self.tree nodeInTheTree:node andRootOfTheTree:self.tree.root])
    {
        [self.tree removeTreeNodeFromParentNode:node];
        
        if (self.bInOperation)
        {
            NSArray *tmpArrayDeletedNodes = [self.tree getAllUnfoldedTreeNodeUnderTreeNode:node andItself:YES];
            
            for (TreeNode *tmpTreeNode in tmpArrayDeletedNodes)
            {
                TreeTableViewCell *tmpCell = tmpTreeNode.cell;
                
                NSIndexPath *tmpIndexPath = [self indexPathForCell:tmpCell];
                
                if (tmpIndexPath)
                {
                    [self deleteRowsAtIndexPaths:@[tmpIndexPath] withRowAnimation:UITableViewRowAnimationRight];
                }
            }
        }
        
        TreeTableViewCell *tmpCellParent = tmpNodeParent.cell;
        if ([self.visibleCells containsObject:tmpCellParent])
            [tmpCellParent updateGUI];
        
        if (self.bInOperation)
            [self RefreshModel];
        else
            [self reloadData];
    }else
        NSLog(@"TreeTableView: Error! the node is not in the tree! ignored.");
    
    return YES;
}

- (BOOL)AddNode:(TreeNode *)node toParentNode:(TreeNode *)parentNode
{
    if (!node || !parentNode) return NO;
    
    if ([self.tree nodeInTheTree:node andRootOfTheTree:self.tree.root])
    {
        NSLog(@"TreeTableView: Error! the node is already in the tree! ignored.");
        return NO;
    }
    
    [self.tree insertTreeNode:node toParent:parentNode];
    
    if (self.bInOperation)
    {
        NSArray *tmpArrayNewModel = [self.tree getAllUnfoldedTreeNodeUnderTreeNode:self.tree.root andItself:INCLUDE_ROOT];
        NSArray *tmpArrayAddedNodes = [self.tree getAllUnfoldedTreeNodeUnderTreeNode:node andItself:YES];
        
        for (TreeNode *tmpTreeNode in tmpArrayAddedNodes)
        {
            int index = [tmpArrayNewModel indexOfObject:tmpTreeNode];
            NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [self insertRowsAtIndexPaths:@[tmpIndexPath] withRowAnimation:UITableViewRowAnimationRight];
        }
        
        [self RefreshModel];
    }else
    {
        [self reloadData];
    }
    
    TreeTableViewCell *tmpCellParent = parentNode.cell;
    if ([self.visibleCells containsObject:tmpCellParent])
        [tmpCellParent updateGUI];
    
    return YES;
}

- (void)beginOperation
{
    if (self.bInOperation) return;
    
    self.bInOperation = YES;
    [self beginUpdates];
}

- (void)endOperation
{
    if (!self.bInOperation) return;
    
    [self endUpdates];
    self.bInOperation = NO;
}

- (Tree *)getTree
{
    return self.tree;
}

- (void)setNewTree:(Tree *)tree
{
    if (self.tree == tree) return;
    
    self.tree = tree;
    
    [self reloadData];
}

- (BOOL)FoldNode:(TreeNode *)node
{
    if (!node) return NO;
    if (node.bFolded) return NO;
    
    [self beginOperation];
    
    NSArray *tmpArrayFoldedNodes = [self.tree getAllUnfoldedTreeNodeUnderTreeNode:node andItself:NO];
    
    for (TreeNode *tmpTreeNode in tmpArrayFoldedNodes)
    {
        int index = [self.arrayMenus indexOfObject:tmpTreeNode];
        
        NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
        
        [self deleteRowsAtIndexPaths:@[tmpIndexPath] withRowAnimation:UITableViewRowAnimationTop];
    }
    
    node.bFolded = !node.bFolded;
    
    [self RefreshModel];
    
    [self endOperation];
    
    TreeTableViewCell *tmpCell = node.cell;
    if ([self.visibleCells containsObject:tmpCell])
        [tmpCell updateGUI];
    
    return YES;
}

- (BOOL)UnfoldNode:(TreeNode *)node
{
    if (!node) return NO;
    if (!node.bFolded) return NO;
    
    [self beginOperation];
    
    node.bFolded = !node.bFolded;
    
    NSArray *tmpArrayUnfoldedNodes = [self.tree getAllUnfoldedTreeNodeUnderTreeNode:node andItself:NO];
    
    NSArray *tmpArrayNewModel = [self.tree getAllUnfoldedTreeNodeUnderTreeNode:self.tree.root andItself:INCLUDE_ROOT];
    
    for (TreeNode *tmpNode in tmpArrayUnfoldedNodes)
    {
        int index = [tmpArrayNewModel indexOfObject:tmpNode];
        NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [self insertRowsAtIndexPaths:@[tmpIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
    }
    
    [self RefreshModel];
    
    [self endOperation];
    
    TreeTableViewCell *tmpCell = node.cell;
    if ([self.visibleCells containsObject:tmpCell])
        [tmpCell updateGUI];
    
    return YES;
}

@end
