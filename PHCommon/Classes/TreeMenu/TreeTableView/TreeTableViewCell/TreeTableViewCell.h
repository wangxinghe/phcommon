//
//  TreeTableViewCell.h
//  TreeMenu_Demo
//
//  Created by Ricol Wang on 2/11/13.
//  Copyright (c) 2013 ricol wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TreeTableViewCellDelegate.h"
#import "TreeNode.h"

@interface TreeTableViewCell : UITableViewCell

@property (weak) id <TreeTableViewCellDelegate> delegate;
@property (weak) TreeNode *node;

- (void)updateGUI;

@end
