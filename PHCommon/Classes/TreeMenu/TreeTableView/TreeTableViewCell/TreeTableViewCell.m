//
//  TreeTableViewCell.m
//  TreeMenu_Demo
//
//  Created by Ricol Wang on 2/11/13.
//  Copyright (c) 2013 ricol wang. All rights reserved.
//

#import "TreeTableViewCell.h"

@interface TreeTableViewCell ()

@property UISwipeGestureRecognizer *swipeToLeftGesture;
@property UISwipeGestureRecognizer *swipeToRightGesture;

- (void)SwipeToLeft:(UISwipeGestureRecognizer *)swipeGesture;
- (void)SwipeToRight:(UISwipeGestureRecognizer *)swipeGesture;

@end

@implementation TreeTableViewCell

#pragma mark - View Life Cycle

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.swipeToLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeToLeft:)];
    self.swipeToLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [self addGestureRecognizer:self.swipeToLeftGesture];
    
    self.swipeToRightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeToRight:)];
    self.swipeToRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self addGestureRecognizer:self.swipeToRightGesture];
}

#pragma mark - IBAction Methods

- (IBAction)btnIconOnTapped:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(TreeTableViewCell:didTapIconWithNode:)])
        [self.delegate TreeTableViewCell:self didTapIconWithNode:self.node];
}

- (void)SwipeToLeft:(UISwipeGestureRecognizer *)swipeGesture
{
    if ([self.delegate respondsToSelector:@selector(TreeTableViewCell:didSwipeToLeftWithNode:)])
        [self.delegate TreeTableViewCell:self didSwipeToLeftWithNode:self.node];
}

- (void)SwipeToRight:(UISwipeGestureRecognizer *)swipeGesture
{
    if ([self.delegate respondsToSelector:@selector(TreeTableViewCell:didSwipeToRightWithNode:)])
        [self.delegate TreeTableViewCell:self didSwipeToRightWithNode:self.node];
}

- (void)updateGUI
{

}

@end
