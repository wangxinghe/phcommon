//
//  TreeNode.m
//  TreeMenu_Demo
//
//  Created by Ricol Wang on 2/11/13.
//  Copyright (c) 2013 ricol wang. All rights reserved.
//

#import "TreeNode.h"

@interface TreeNode ()

+ (NSString *)GetUniqueTimeStamp;
+ (NSString *)GetUniqueNumberString;

@end

@implementation TreeNode

- (id)init
{
    self = [super init];
    if (self)
    {
        self.mutableDictContent = [NSMutableDictionary new];
        self.mutableDictContent[@"title"] = @"";
        self.mutableArraySubTreeNodes = [NSMutableArray new];
        self.parentTreeNode = nil;
        self.bFolded = NO;
        self.bCheckedSymbol = YES;
        self.identifier = [TreeNode GetUniqueNumberString];
    }
    return self;
}

- (id)initWithTitle:(NSString *)title
{
    self = [super init];
    if (self)
    {
        self.mutableDictContent = [NSMutableDictionary new];
        self.mutableDictContent[@"title"] = [title copy];
        self.mutableArraySubTreeNodes = [NSMutableArray new];
        self.parentTreeNode = nil;
        self.bFolded = NO;
        self.bCheckedSymbol = YES;
        self.identifier = [TreeNode GetUniqueNumberString];
    }
    return self;
}

- (int)getLevel
{
    int level = 0;
    
    TreeNode *tmpTreeNode = self.parentTreeNode;
    
    while (tmpTreeNode)
    {
        level++;
        tmpTreeNode = tmpTreeNode.parentTreeNode;
    }
    
    return level;
}

- (void)setIcon:(NSString *)imageFileName
{
    UIImage *tmpImage = [UIImage imageNamed:imageFileName];
    if (!tmpImage)
        [self.mutableDictContent removeObjectForKey:@"icon"];
    else
        self.mutableDictContent[@"icon"] = tmpImage;
}

- (NSString *)getTitle
{
    return self.mutableDictContent[@"title"];
}

- (UIImage *)getIcon
{
    return self.mutableDictContent[@"icon"];
}

- (BOOL)isLeaf
{
    return self.mutableArraySubTreeNodes.count <= 0;
}

- (int)numberOfChildren
{
    int number = 0;
    
    number += self.mutableArraySubTreeNodes.count;
    
    if (self.mutableArraySubTreeNodes.count > 0)
    {
        for (TreeNode *tmpNode in self.mutableArraySubTreeNodes)
        {
            number += [tmpNode numberOfChildren];
        }
    }
    
    return number;
}

- (NSArray *)getAllChildren
{
    return self.mutableArraySubTreeNodes;
}

- (void)setTitle:(NSString *)title
{
    self.mutableDictContent[@"title"] = [title copy];
}

#pragma mark - Private Static Methods

+ (NSString *)GetUniqueTimeStamp
{
    return [[NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]] stringValue];
}

+ (NSString *)GetUniqueNumberString
{
    NSString *tmpStr = [TreeNode GetUniqueTimeStamp];
    NSString *tmpStrNew = [[tmpStr stringByReplacingOccurrencesOfString:@"." withString:@""] copy];
    return tmpStrNew;
}

@end
