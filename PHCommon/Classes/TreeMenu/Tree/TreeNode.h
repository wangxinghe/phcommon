//
//  TreeNode.h
//  TreeMenu_Demo
//
//  Created by Ricol Wang on 2/11/13.
//  Copyright (c) 2013 ricol wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TreeTableViewCell.h"

@interface TreeNode : NSObject

@property (copy) NSString *identifier;
@property (nonatomic) BOOL bFolded;
@property BOOL bSeperator;
@property BOOL bChecked;
@property BOOL bCheckedMenu;
@property BOOL bCheckedSymbol;
@property (weak) TreeNode *parentTreeNode;
@property (strong) NSMutableArray *mutableArraySubTreeNodes;
@property (strong) NSMutableDictionary *mutableDictContent;
@property (strong) TreeTableViewCell *cell;

- (id)initWithTitle:(NSString *)title;
- (int)getLevel;
- (void)setIcon:(NSString *)imageFileName;
- (BOOL)isLeaf;
- (NSString *)getTitle;
- (void)setTitle:(NSString *)title;
- (UIImage *)getIcon;
- (int)numberOfChildren;
- (NSArray *)getAllChildren;

@end
