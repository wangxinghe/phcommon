//
//  Tree.h
//  TreeMenu_Demo
//
//  Created by Ricol Wang on 2/11/13.
//  Copyright (c) 2013 ricol wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TreeNode.h"

@interface Tree : NSObject

@property (strong) TreeNode *root;

- (id)init;

- (void)insertTreeNode:(TreeNode *)treeNode toParent:(TreeNode *)treeNodeParent;
- (void)removeTreeNode:(TreeNode *)treeNode;
- (TreeNode *)removeTreeNodeFromParentNode:(TreeNode *)treeNode;
- (void)PrintTreeBefore:(TreeNode *)treeNode;
- (void)PrintTreeAfter:(TreeNode *)treeNode;
- (NSArray *)getAllTreeNode:(TreeNode *)treeNode;
- (NSArray *)getAllUnfoldedTreeNodeUnderTreeNode:(TreeNode *)treeNode andItself:(BOOL)bInclude;
- (BOOL)nodeInTheTree:(TreeNode *)treeNode andRootOfTheTree:(TreeNode *)root;
- (NSArray *)getTreeNodesByTitle:(NSString *)title andParentTitle:(NSString *)parentTitle;
- (TreeNode *)getTreeNodeByIdentifier:(NSString *)identifier;

+ (NSArray *)arrayWithAlphabeticalOrder:(NSArray *)arrayTreeNodes;

@end
