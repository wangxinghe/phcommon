//
//  CalendarMonthView.h
//  Sidekick
//
//  Created by Devin Ross Modified by Ricol Wang on 28/05/13.
//
//

#import <UIKit/UIKit.h>
#import "CalendarMonthViewDataSource.h"
#import "CalendarMonthViewDelegate.h"

@class CalendarMonthTile;

@interface CalendarMonthView : UIView

@property (nonatomic, weak) id<CalendarMonthViewDelegate> delegate;
@property (nonatomic, weak) id<CalendarMonthViewDataSource> dataSource;

- (NSDate *)dateSelected;
- (NSDate *)monthDate;
- (void)reload:(BOOL)animate;
- (void)setNewWidth:(float)newWidth andNewHeight:(float)newHeight withAnimation:(BOOL)bAnimation;
- (void)setHeaderHeight:(float)newHeaderHeight withAnimation:(BOOL)bAnimation;
- (void)setWeekHeight:(float)newWeekHeight withAnimation:(BOOL)bAnimation;

@end
