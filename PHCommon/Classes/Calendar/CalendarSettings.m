//
//  CalendarSettings.m
//  PHCommon
//
//  Created by Ricol Wang on 3/08/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import "CalendarSettings.h"
#import "ISingleton.h"

@implementation CalendarSettings

+ (instancetype)sharedInstance
{
    static CalendarSettings *instance = nil;
    if (!instance)
        instance = [CalendarSettings new];
    return instance;
}

#pragma mark - Getter

- (UIColor *)colorToday
{
    if (!_colorToday)
        _colorToday = [UIColor blueColor];
    
    return _colorToday;
}

- (UIColor *)colorDot
{
    if (!_colorDot)
        _colorDot = [UIColor blueColor];
    
    return _colorDot;
}

- (UIImage *)imageSelection
{
    if (!_imageSelection)
    {
        NSString *strPath = [self.theBundle pathForResource:@"Month Calendar Date Tile Selected" ofType:@"png"];
        _imageSelection = [UIImage imageWithContentsOfFile:strPath];
    }
    
    return _imageSelection;
}

- (NSBundle *)theBundle
{
    if (!_theBundle)
    {
        _theBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"Calendar" ofType:@"bundle"]];
    }
    
    return _theBundle;
}

- (NSNumber *)fontSizeDate
{
    if (!_fontSizeDate)
        _fontSizeDate = @(12);
    
    return _fontSizeDate;
}

- (NSNumber *)fontSizeDot
{
    if (!_fontSizeDot)
        _fontSizeDot = @(10);
    
    return _fontSizeDot;
}

- (UIColor *)colorDate
{
    if (!_colorDate)
        _colorDate = [UIColor blackColor];
    
    return _colorDate;
}

- (UIColor *)colorPreviousDays
{
    if (!_colorPreviousDays)
        _colorPreviousDays = [UIColor grayColor];
    
    return _colorPreviousDays;
}

- (UIColor *)colorNextDays
{
    if (!_colorNextDays)
        _colorNextDays = [UIColor grayColor];
    
    return _colorNextDays;
}

@end
