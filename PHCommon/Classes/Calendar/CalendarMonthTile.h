//
//  CalendarMonthTile.h
//  Sidekick
//
//  Created by Devin Ross Modified by Ricol Wang on 28/05/13.
//
//

#import <UIKit/UIKit.h>
#import "CalendarMonthTileDatasource.h"
#import "CalendarMonthTileDelegate.h"

@interface CalendarMonthTile : UIView

{
    SEL action;
}

@property int firstOfPrev, lastOfPrev;
@property int today;
@property int selectedDay, selectedPortion;
@property int firstWeekday, daysInMonth;
@property BOOL bMarkWasOnToday;
@property BOOL bStartOnSunday;
@property float width;
@property float height;

@property (strong) NSDate *monthDate;
@property (weak) id <CalendarMonthTileDatasource> datasource;
@property (weak) id <CalendarMonthTileDelegate> delegate;

- (id)initWithMonth:(NSDate *)date startDayOnSunday:(BOOL)sunday andWidth:(float)newWidth andHeight:(float)newHeight andDataSource:(id <CalendarMonthTileDatasource>)dataSource andDelegate:(id <CalendarMonthTileDelegate>)delegate;
- (void)setTarget:(id)target action:(SEL)action;
- (void)selectDay:(int)day;
- (NSDate *)dateSelected;
+ (NSArray *)rangeOfDatesInMonthGrid:(NSDate *)date startOnSunday:(BOOL)sunday;

@end
