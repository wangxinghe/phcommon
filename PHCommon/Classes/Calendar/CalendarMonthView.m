//
//  CalendarMonthView.m
//  Sidekick
//
//  Created by Devin Ross Modified by Ricol Wang on 28/05/13.
//
//

#import "CalendarMonthView.h"
#import "CalendarMonthTile.h"
#import "NSDate+TKCategory.h"
#import "CalendarMonthView+private.h"
#import "CalendarConsts.h"
#import "utils.h"

#define DEFAULT_WIDTH 200
#define DEFAULT_HEIGHT 200

#define FONT_SIZE_HEADER_WEEK 15
#define FONT_SIZE_HEADER_DATE 20

#define HEIGHT_HEADER_DATE 40
#define HEIGHT_HEADER_WEEK 40

@interface CalendarMonthView () <CalendarMonthTileDatasource, CalendarMonthTileDelegate>

{
    float width, height;
    float cellWidth, cellHeight;
    float headerHeight;
    float weekHeight;
}

@property (strong) NSBundle *theBundle;
@property (nonatomic, strong) CalendarMonthTile *currentTile, *oldTile;
@property (nonatomic, strong) UIButton *btnLeftArrow, *btnRightArrow;
@property (nonatomic, strong) UILabel *lblMonthYear;
@property (nonatomic, strong) UIView *tilebox;
@property (nonatomic, strong) UIButton *btnToday;
@property BOOL bSundayFirst;

@end

@implementation CalendarMonthView

#pragma mark - View Life Cycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.clipsToBounds = true;
    width = DEFAULT_WIDTH;
    height = DEFAULT_HEIGHT;
    headerHeight = HEIGHT_HEADER_DATE;
    weekHeight = HEIGHT_HEADER_WEEK;
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"Calendar" ofType:@"bundle"];
    self.theBundle = [NSBundle bundleWithPath:bundlePath];

    [self initWithSundayAsFirst:YES andWidth:self.frame.size.width andHeight:self.frame.size.height withAnimation:NO];
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.clipsToBounds = true;
        width = DEFAULT_WIDTH;
        height = DEFAULT_HEIGHT;
        headerHeight = HEIGHT_HEADER_DATE;
        weekHeight = HEIGHT_HEADER_WEEK;
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"Calendar" ofType:@"bundle"];
        self.theBundle = [NSBundle bundleWithPath:bundlePath];
        [self initWithSundayAsFirst:YES andWidth:width andHeight:height withAnimation:NO];
    }
    return self;
}

#pragma mark - Getter

- (UILabel *)lblMonthYear
{
    if (_lblMonthYear == nil)
    {
        _lblMonthYear = [[UILabel alloc] initWithFrame:CGRectMake(cellWidth, 0, self.tilebox.frame.size.width - cellWidth * 2, headerHeight)];

        _lblMonthYear.textAlignment = NSTextAlignmentCenter;

#ifdef _COLOR
        _lblMonthYear.backgroundColor = [utils randomColor];
#else
        _lblMonthYear.backgroundColor = [UIColor clearColor];
#endif
        _lblMonthYear.font = [UIFont boldSystemFontOfSize:FONT_SIZE_HEADER_DATE];
        _lblMonthYear.textColor = [UIColor colorWithRed:59 / 255. green:73 / 255. blue:88 / 255. alpha:1];
    }
    return _lblMonthYear;
}

- (UIButton *)btnLeftArrow
{
    if (_btnLeftArrow == nil)
    {
        _btnLeftArrow = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnLeftArrow.tag = 0;
        [_btnLeftArrow addTarget:self action:@selector(changeMonth:) forControlEvents:UIControlEventTouchUpInside];

#ifdef _COLOR
        _btnLeftArrow.backgroundColor = [utils randomColor];
#else
        _btnLeftArrow.backgroundColor = [UIColor clearColor];
#endif
        [_btnLeftArrow setImage:[UIImage imageWithContentsOfFile:[self.theBundle pathForResource:@"Month Calendar Left Arrow" ofType:@"png"]]
                       forState:0];

        _btnLeftArrow.frame = CGRectMake(0, 0, cellWidth, headerHeight);
    }
    return _btnLeftArrow;
}

- (UIButton *)btnRightArrow
{
    if (_btnRightArrow == nil)
    {
        _btnRightArrow = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnRightArrow.tag = 1;
        [_btnRightArrow addTarget:self action:@selector(changeMonth:) forControlEvents:UIControlEventTouchUpInside];
        _btnRightArrow.frame = CGRectMake(7 * cellWidth - cellWidth, 0, cellWidth, headerHeight);

#ifdef _COLOR
        _btnRightArrow.backgroundColor = [utils randomColor];
#else
        _btnRightArrow.backgroundColor = [UIColor clearColor];
#endif
        [_btnRightArrow setImage:[UIImage imageWithContentsOfFile:[self.theBundle pathForResource:@"Month Calendar Right Arrow" ofType:@"png"]]
                        forState:0];
    }
    return _btnRightArrow;
}

- (UIView *)tilebox
{
    if (_tilebox == nil)
    {
        _tilebox = [[UIView alloc] initWithFrame:CGRectMake(0, headerHeight + weekHeight, 7 * cellWidth, self.currentTile.frame.size.height)];
#ifdef _COLOR
        _tilebox.backgroundColor = [utils randomColor];
#else
        _tilebox.backgroundColor = [UIColor clearColor];
#endif
    }
    return _tilebox;
}

#pragma mark - CalendarMonthTileDatasource

- (BOOL)calendarMonthTile:(CalendarMonthTile *)monthTile markForDate:(NSDate *)date
{
    return [self.dataSource calendarMonthView:self markForDate:date];
}

#pragma mark - CalendarMonthTileDelegate

- (void)calendarMonthTile:(CalendarMonthTile *)monthTile didSelectDate:(NSDate *)d
{
    //    [self.delegate calendarMonthView:self didSelectDate:d];
}

#pragma mark - Public Methods

- (void)setHeaderHeight:(float)newHeaderHeight withAnimation:(BOOL)bAnimation
{
    headerHeight = newHeaderHeight;
    [self initWithSundayAsFirst:YES andWidth:width andHeight:height withAnimation:bAnimation];
}

- (void)setNewWidth:(float)newWidth andNewHeight:(float)newHeight withAnimation:(BOOL)bAnimation
{
    [self initWithSundayAsFirst:YES andWidth:newWidth andHeight:newHeight withAnimation:bAnimation];
}

- (void)setWeekHeight:(float)newWeekHeight withAnimation:(BOOL)bAnimation
{
    weekHeight = newWeekHeight;
    [self initWithSundayAsFirst:YES andWidth:width andHeight:height withAnimation:bAnimation];
}

- (void)reload:(BOOL)animate
{
    [self setNewWidth:width andNewHeight:height withAnimation:animate];
}

- (NSDate *)dateSelected
{
    return [self.currentTile dateSelected];
}

- (NSDate *)monthDate
{
    return [self.currentTile monthDate];
}

#pragma mark - Private Methods

- (void)initWithSundayAsFirst:(BOOL)s andWidth:(float)newWidth andHeight:(float)newHeight withAnimation:(BOOL)bAnimation
{
    for (UIView *tmpView in self.subviews)
        [tmpView removeFromSuperview];
    self.tilebox = NULL;
    self.lblMonthYear = NULL;
    self.btnLeftArrow = NULL;
    self.btnRightArrow = NULL;

    width = newWidth;
    height = newHeight;
    cellWidth = width / 7;
    cellHeight = height / 7;

    CGRect tmpRect = self.frame;
    tmpRect.size.width = width;
    tmpRect.size.height = height;
    self.frame = tmpRect;

    [self addSubview:self.tilebox];
    [self addSubview:self.lblMonthYear];
    [self addSubview:self.btnLeftArrow];
    [self addSubview:self.btnRightArrow];

    self.bSundayFirst = s;
    self.currentTile = [[CalendarMonthTile alloc] initWithMonth:[self firstOfMonthFromDate:[NSDate date]]

                                               startDayOnSunday:YES
                                                       andWidth:cellWidth
                                                      andHeight:cellHeight
                                                  andDataSource:self
                                                    andDelegate:self];
    self.currentTile.backgroundColor = [UIColor clearColor];
    [self.currentTile setTarget:self action:@selector(tile:)];

    [self.tilebox addSubview:self.currentTile];

    NSDate *date = [NSDate date];
    self.lblMonthYear.text = [NSString stringWithFormat:@"%@ %@", [date month], [date year]];

#ifdef _COLOR
    self.backgroundColor = [utils randomColor];
#else
    self.backgroundColor = [UIColor clearColor];
#endif

    // create labels from monday to bSundayFirst
    NSArray *ar;
    if (self.bSundayFirst)
        ar = [NSArray arrayWithObjects:kSun, kMon, kTue, kWed, kThu, kFri, kSat, nil];
    else
        ar = [NSArray arrayWithObjects:kMon, kTue, kWed, kThu, kFri, kSat, kSun, nil];
    int i = 0;
    for (NSString *s in ar)
    {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(cellWidth * i, headerHeight, cellWidth, weekHeight)];
        [self addSubview:label];
        label.text = s;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:FONT_SIZE_HEADER_WEEK];
#ifdef _COLOR
        label.backgroundColor = [utils randomColor];
#else
        label.backgroundColor = [UIColor clearColor];
#endif
        label.textColor = [UIColor colorWithRed:59 / 255. green:73 / 255. blue:88 / 255. alpha:1];
        i++;
    }

    // create button which navigates to today's date
    self.btnToday = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnToday.backgroundColor = [UIColor clearColor];
    self.btnToday.frame = self.lblMonthYear.frame;
    [self.btnToday addTarget:self action:@selector(NavigateToToday:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnToday];

    [self gotoToday:bAnimation];
}

- (void)changeMonthAnimation:(UIView *)sender
{
    BOOL isNext = (sender.tag == 1);

    NSDate *nextMonth = isNext ? [self nextMonthFromDate:self.currentTile.monthDate] : [self previousMonthFromDate:self.currentTile.monthDate];

    CalendarMonthTile *newTile = [[CalendarMonthTile alloc] initWithMonth:nextMonth
                                                         startDayOnSunday:self.bSundayFirst
                                                                 andWidth:cellWidth
                                                                andHeight:cellHeight
                                                            andDataSource:self
                                                              andDelegate:self];
    [newTile setTarget:self action:@selector(tile:)];
    newTile.backgroundColor = [UIColor clearColor];

    float del = isNext ? width : -width;
    newTile.frame = CGRectMake(del, 0, newTile.frame.size.width, newTile.frame.size.height);
    [self.tilebox addSubview:newTile];
    [self.tilebox bringSubviewToFront:self.currentTile];

    self.userInteractionEnabled = NO;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDidStopSelector:@selector(animationEnded)];
    [UIView setAnimationDuration:0.5];

    self.currentTile.alpha = 0.0;

    if (isNext)
    {
        self.currentTile.frame = CGRectMake(-del, 0, self.currentTile.frame.size.width, self.currentTile.frame.size.height);
        newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
        self.tilebox.frame =
            CGRectMake(self.tilebox.frame.origin.x, self.tilebox.frame.origin.y, self.tilebox.frame.size.width, newTile.frame.size.height);
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width,
                                self.tilebox.frame.size.height + self.tilebox.frame.origin.y);
    }
    else
    {
        newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
        self.tilebox.frame =
            CGRectMake(self.tilebox.frame.origin.x, self.tilebox.frame.origin.y, self.tilebox.frame.size.width, newTile.frame.size.height);
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width,
                                self.tilebox.frame.size.height + self.tilebox.frame.origin.y);
        self.currentTile.frame = CGRectMake(-del, 0, self.currentTile.frame.size.width, self.currentTile.frame.size.height);
    }

    [UIView commitAnimations];

    self.oldTile = self.currentTile;
    self.currentTile = newTile;
    self.lblMonthYear.text = [NSString stringWithFormat:@"%@ %@", [nextMonth month], [nextMonth year]];
}

- (void)changeMonth:(UIButton *)sender
{
    [self changeMonthAnimation:sender];

    if ([self.delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:)])
        [self.delegate calendarMonthView:self monthDidChange:self.currentTile.monthDate];
}

- (void)animationEnded
{
    self.userInteractionEnabled = YES;
    self.oldTile = nil;
}

- (void)tile:(NSArray *)ar
{
    if ([ar count] < 2)
    {
        NSDate *d = [self.currentTile monthDate];
        TKDateInformation info = [d dateInformation];
        info.day = [[ar objectAtIndex:0] intValue];

        NSDate *select = [NSDate dateFromDateInformation:info];
        if ([self.delegate respondsToSelector:@selector(calendarMonthView:didSelectDate:)])
            [self.delegate calendarMonthView:self didSelectDate:select];
    }
    else
    {
        int direction = [[ar lastObject] intValue];
        UIButton *b = direction > 1 ? self.btnRightArrow : self.btnLeftArrow;

        [self changeMonthAnimation:b];

        int day = [[ar objectAtIndex:0] intValue];
        [self.currentTile selectDay:day];

        // thanks rafael
        TKDateInformation info = [[self.currentTile monthDate] dateInformation];
        info.day = day;
        NSDate *dateForMonth = [NSDate dateFromDateInformation:info];
        [self.currentTile selectDay:day];

        if ([self.delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:)])
            [self.delegate calendarMonthView:self monthDidChange:dateForMonth];

        NSDate *select = [NSDate dateFromDateInformation:info];
        if ([self.delegate respondsToSelector:@selector(calendarMonthView:didSelectDate:)])
            [self.delegate calendarMonthView:self didSelectDate:select];
    }
}

- (void)NavigateToToday:(id)sender
{
    [self gotoToday:YES];
}

- (void)gotoToday:(BOOL)bAnimation
{
    NSDate *tmpNow = [NSDate date];
    CalendarMonthTile *newTile = [[CalendarMonthTile alloc] initWithMonth:[self firstOfMonthFromDate:tmpNow]

                                                         startDayOnSunday:YES
                                                                 andWidth:cellWidth
                                                                andHeight:cellHeight
                                                            andDataSource:self
                                                              andDelegate:self];
    newTile.backgroundColor = [UIColor clearColor];

    [newTile setTarget:self action:@selector(tile:)];

    [self.tilebox addSubview:newTile];
    [self.tilebox bringSubviewToFront:self.currentTile];

    if (bAnimation)
    {

        self.userInteractionEnabled = NO;

        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDidStopSelector:@selector(animationEnded)];
        [UIView setAnimationDuration:0.4];

        self.currentTile.alpha = 0.0;

        BOOL isNext = NO;

        int overlap = 0;

        NSArray *dates = [CalendarMonthTile rangeOfDatesInMonthGrid:tmpNow startOnSunday:self.bSundayFirst];

        if (isNext)
        {
            overlap = [newTile.monthDate isEqualToDate:[dates objectAtIndex:0]] ? 0 : width;
        }
        else
        {
            overlap = [self.currentTile.monthDate compare:[dates lastObject]] != NSOrderedDescending ? width : 0;
        }

        float y = isNext ? self.currentTile.bounds.size.height - overlap : newTile.bounds.size.height * -1 + overlap;

        newTile.frame = CGRectMake(0, y, newTile.frame.size.width, newTile.frame.size.height);

        if (isNext)
        {
            self.currentTile.frame = CGRectMake(0, -1 * self.currentTile.bounds.size.height + overlap, self.currentTile.frame.size.width,
                                                self.currentTile.frame.size.height);
            newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
            self.tilebox.frame =
                CGRectMake(self.tilebox.frame.origin.x, self.tilebox.frame.origin.y, self.tilebox.frame.size.width, newTile.frame.size.height);
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width,
                                    self.tilebox.frame.size.height + self.tilebox.frame.origin.y);
        }
        else
        {
            newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
            self.tilebox.frame =
                CGRectMake(self.tilebox.frame.origin.x, self.tilebox.frame.origin.y, self.tilebox.frame.size.width, newTile.frame.size.height);
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width,
                                    self.tilebox.frame.size.height + self.tilebox.frame.origin.y);
            self.currentTile.frame =
                CGRectMake(0, newTile.frame.size.height - overlap, self.currentTile.frame.size.width, self.currentTile.frame.size.height);
        }

        [UIView commitAnimations];

        self.oldTile = self.currentTile;
        self.currentTile = newTile;
    }
    else
    {
        self.currentTile.alpha = 0.0;
        
        BOOL isNext = NO;
        
        int overlap = 0;
        
        NSArray *dates = [CalendarMonthTile rangeOfDatesInMonthGrid:tmpNow startOnSunday:self.bSundayFirst];
        
        if (isNext)
        {
            overlap = [newTile.monthDate isEqualToDate:[dates objectAtIndex:0]] ? 0 : width;
        }
        else
        {
            overlap = [self.currentTile.monthDate compare:[dates lastObject]] != NSOrderedDescending ? width : 0;
        }
        
        float y = isNext ? self.currentTile.bounds.size.height - overlap : newTile.bounds.size.height * -1 + overlap;
        
        newTile.frame = CGRectMake(0, y, newTile.frame.size.width, newTile.frame.size.height);
        
        if (isNext)
        {
            self.currentTile.frame =
            CGRectMake(0, -1 * self.currentTile.bounds.size.height + overlap, self.currentTile.frame.size.width, self.currentTile.frame.size.height);
            newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
            self.tilebox.frame =
            CGRectMake(self.tilebox.frame.origin.x, self.tilebox.frame.origin.y, self.tilebox.frame.size.width, newTile.frame.size.height);
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width,
                                    self.tilebox.frame.size.height + self.tilebox.frame.origin.y);
        }
        else
        {
            newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
            self.tilebox.frame =
            CGRectMake(self.tilebox.frame.origin.x, self.tilebox.frame.origin.y, self.tilebox.frame.size.width, newTile.frame.size.height);
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width,
                                    self.tilebox.frame.size.height + self.tilebox.frame.origin.y);
            self.currentTile.frame =
            CGRectMake(0, newTile.frame.size.height - overlap, self.currentTile.frame.size.width, self.currentTile.frame.size.height);
        }
        
        self.oldTile = self.currentTile;
        self.currentTile = newTile;
    }

    self.lblMonthYear.text = [NSString stringWithFormat:@"%@ %@", [tmpNow month], [tmpNow year]];

    if ([self.delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:)])
        [self.delegate calendarMonthView:self monthDidChange:self.currentTile.monthDate];
}

@end
