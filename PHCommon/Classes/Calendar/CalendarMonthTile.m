//
//  CalendarMonthTile.m
//  Sidekick
//
//  Created by Devin Ross Modified by Ricol Wang on 28/05/13.
//
//

#import "CalendarMonthTile.h"
#import "NSDate+TKCategory.h"
#import "CalendarConsts.h"
#import "utils.h"
#import "CalendarSettings.h"

@interface CalendarMonthTile ()

@property (nonatomic, strong) UILabel *lblDot;
@property (nonatomic, strong) UIImageView *ImageViewSelected;
@property (strong) NSObject *target;

@end

@implementation CalendarMonthTile

#pragma mark - View Override Methods

- (id)initWithMonth:(NSDate *)date startDayOnSunday:(BOOL)sunday andWidth:(float)newWidth andHeight:(float)newHeight andDataSource:(id <CalendarMonthTileDatasource>)dataSource andDelegate:(id <CalendarMonthTileDelegate>)delegate
{
    self.datasource = dataSource;
    self.delegate = delegate;
    self.width = newWidth;
    self.height = newHeight;
#ifdef _COLOR
    self.backgroundColor = [utils randomColor];
#else
    self.backgroundColor = [UIColor clearColor];
#endif
    
    self.firstOfPrev = -1;
    self.monthDate = date;
    self.bStartOnSunday = sunday;
    
    TKDateInformation dateInfo = [self.monthDate dateInformation];
    self.firstWeekday = dateInfo.weekday;
    self.daysInMonth = [date daysInMonth];
    int row = (self.daysInMonth + dateInfo.weekday - 1);
    row = (row / 7) + ((row % 7 == 0) ? 0 : 1);
    
    TKDateInformation todayInfo = [[NSDate date] dateInformation];
    self.today = dateInfo.month == todayInfo.month && dateInfo.year == todayInfo.year ? todayInfo.day : 0;
    
    if (self.firstWeekday > 1)
    {
        dateInfo.month--;
        if (dateInfo.month < 1)
        {
            dateInfo.month = 12;
            dateInfo.year--;
        }
        NSDate *previousMonth = [NSDate dateFromDateInformation:dateInfo];
        int preDayCnt = [previousMonth daysInMonth];
        self.firstOfPrev = preDayCnt - self.firstWeekday + 2;
        self.lastOfPrev = preDayCnt;
    }
    
    if (![super initWithFrame:CGRectMake(0, 0, self.width * 7, self.height * row)])
        return nil;
    
#ifdef _COLOR
    self.ImageViewSelected.backgroundColor = [utils randomColor];
#else
    self.ImageViewSelected.backgroundColor = [UIColor clearColor];
#endif
    
    self.multipleTouchEnabled = NO;
    
    return self;
}


- (void)drawRect:(CGRect)rect
{
    CGRect r = rect;
    int index = 0;
    
    UIFont *font_date = [UIFont boldSystemFontOfSize:[[CalendarSettings sharedInstance].fontSizeDate intValue]];
    UIFont *font_dot = [UIFont boldSystemFontOfSize:[[CalendarSettings sharedInstance].fontSizeDot intValue]];

    if (self.firstOfPrev > 0)
    {
        for (int i = self.firstOfPrev; i <= self.lastOfPrev; i++)
        {
            [[CalendarSettings sharedInstance].colorPreviousDays set];
            
            r = [self rectForCellAtIndex:index];
            
            // get the NSDate for the previous month
            TKDateInformation tmpDateInfor = [self.monthDate dateInformation];
            tmpDateInfor.day = i;
            tmpDateInfor.month--;
            if (tmpDateInfor.month < 1)
                tmpDateInfor.month = 12;
            NSDate *tmpDate = [NSDate dateFromDateInformation:tmpDateInfor];
            
            // get number of jobs for the date
            BOOL dot = [self.datasource calendarMonthTile:self markForDate:tmpDate];
            
            [self drawTileInRect:r day:i font:font_date font2:font_dot withDot:dot];
            
            index++;
        }
    }
    
    for (int i = 1; i <= self.daysInMonth; i++)
    {
        [[CalendarSettings sharedInstance].colorDate set];
        
        r = [self rectForCellAtIndex:index];
        
        if (i == self.today)
            [[CalendarSettings sharedInstance].colorToday set];
        
        // get current NSDate
        TKDateInformation tmpDateInfor = [self.monthDate dateInformation];
        tmpDateInfor.day = i;
        NSDate *tmpDate = [NSDate dateFromDateInformation:tmpDateInfor];
        
        // get number of jobs for the date
        BOOL dot = [self.datasource calendarMonthTile:self markForDate:tmpDate];
        
        // print the tile with number of jobs for the date
        [self drawTileInRect:r day:i font:font_date font2:font_dot withDot:dot];
        
        index++;
    }
    
    int i = 1;
    while (index % 7 != 0)
    {
        [[CalendarSettings sharedInstance].colorNextDays set];
        
        r = [self rectForCellAtIndex:index];
        
        // get the NSDate for the previous month
        TKDateInformation tmpDateInfor = [self.monthDate dateInformation];
        tmpDateInfor.day = i;
        tmpDateInfor.month++;
        if (tmpDateInfor.month > 12)
            tmpDateInfor.month = 1;
        NSDate *tmpDate = [NSDate dateFromDateInformation:tmpDateInfor];
        
        // get number of jobs for the date
        BOOL dot = [self.datasource calendarMonthTile:self markForDate:tmpDate];
        
        [self drawTileInRect:r day:i font:font_date font2:font_dot withDot:dot];
        
        i++;
        index++;
    }
    
    CGRect tmpRect = self.ImageViewSelected.bounds;
    tmpRect.origin.y -= (self.height / 3.0) / 2;
}

#pragma mark - Getter

- (UILabel *)lblDot
{
    if (_lblDot == nil)
    {
        CGRect r = self.ImageViewSelected.bounds;
        r.origin.y = r.size.height - (self.height / 3.0f) + self.height / 3.0f;
        r.size.height = (self.height / 3.0f);
        r.size.width = self.width;
        _lblDot = [[UILabel alloc] initWithFrame:r];
        
        _lblDot.text = @"•";
        _lblDot.textColor = [CalendarSettings sharedInstance].colorDot;
#ifdef _COLOR
        _lblDot.backgroundColor = [utils randomColor];
#else
        _lblDot.backgroundColor = [UIColor clearColor];
#endif
        _lblDot.font = [UIFont boldSystemFontOfSize:[[CalendarSettings sharedInstance].fontSizeDot intValue]];
        _lblDot.textAlignment = NSTextAlignmentCenter;
    }
    return _lblDot;
}

- (UIImageView *)ImageViewSelected
{
    if (_ImageViewSelected == nil)
    {
        _ImageViewSelected = [[UIImageView alloc]
                              initWithImage:[CalendarSettings sharedInstance].imageSelection];
        _ImageViewSelected.contentMode = UIViewContentModeScaleToFill;
#ifdef _COLOR
        _ImageViewSelected.backgroundColor = [utils randomColor];
#else
        _ImageViewSelected.backgroundColor = [UIColor clearColor];
#endif
    }
    return _ImageViewSelected;
}


#pragma mark - Private Methods

- (CGRect)rectForCellAtIndex:(int)index
{
    int row = index / 7;
    int col = index % 7;

    return CGRectMake(col * self.width, row * self.height, self.width, self.height);
}

- (void)drawTileInRect:(CGRect)r day:(int)day font:(UIFont *)f1 font2:(UIFont *)f2 withDot:(BOOL)dot
{
    NSString *str = [NSString stringWithFormat:@"%d", day];

    CGRect tmpR = r;
    tmpR.origin.y += tmpR.size.height / 3.0f;
    tmpR.size.height -= (tmpR.size.height / 3.0f) * 2;
    
    [str drawInRect:tmpR withFont:f1 lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentCenter];

    if (dot)
    {
        [[CalendarSettings sharedInstance].colorDot set];
        tmpR.size.height = self.height / 3.0f;
        tmpR.origin.y = self.height * (2.0 / 3.0f) + r.origin.y;
        [@"*" drawInRect:tmpR withFont:f2 lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentCenter];
    }
    
#ifdef _COLOR
    [self drawARect:r];
#endif
    
}

- (void)reactToTouch:(UITouch *)touch down:(BOOL)down
{
    CGPoint p = [touch locationInView:self];
    if (p.y > self.bounds.size.height || p.y < 0)
        return;

    int column = p.x / self.width, row = p.y / self.height;

    int day = 1, portion = 0;

//    if (row == (int)(self.bounds.size.height / self.height))
//        row--;

    if (row == 0 && column < self.firstWeekday - 1)
    {
        day = self.firstOfPrev + column;
    }
    else
    {
        portion = 1;
        day = row * 7 + column - self.firstWeekday + 2;
    }
    if (portion > 0 && day > self.daysInMonth)
    {
        portion = 2;
        day = day - self.daysInMonth;
    }

    if (portion != 1)
    {
        self.ImageViewSelected.image = [CalendarSettings sharedInstance].imageSelection;

        self.bMarkWasOnToday = YES;
    }
    else if (portion == 1 && day == self.today)
    {

        self.ImageViewSelected.image = [CalendarSettings sharedInstance].imageSelection;

        self.bMarkWasOnToday = YES;
    }
    else if (self.bMarkWasOnToday)
    {

        self.ImageViewSelected.image = [CalendarSettings sharedInstance].imageSelection;

        self.bMarkWasOnToday = NO;
    }

    [self addSubview:self.ImageViewSelected];

    CGRect r = [self rectForCellAtIndex:row * 7 + column];

    self.ImageViewSelected.frame = r;

    if (day == self.selectedDay && self.selectedPortion == portion)
        return;

    if (portion == 1)
    {
        self.selectedDay = day;
        self.selectedPortion = portion;

        [self.target performSelector:action withObject:[NSArray arrayWithObject:[NSNumber numberWithInt:day]]];
    }
    else if (down)
    {
        [self.target performSelector:action
                          withObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:day], [NSNumber numberWithInt:portion], nil]];
        self.selectedDay = day;
        self.selectedPortion = portion;
    }
    
    [self.delegate calendarMonthTile:self didSelectDate:[self dateSelected]];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self reactToTouch:[touches anyObject] down:NO];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self reactToTouch:[touches anyObject] down:NO];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self reactToTouch:[touches anyObject] down:YES];
}

- (void)drawARect:(CGRect)aRect
{
    CGContextRef context=UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, aRect.origin.x, aRect.origin.y);
    CGContextAddLineToPoint(context, aRect.origin.x + aRect.size.width - 1, aRect.origin.y);
    CGContextAddLineToPoint(context, aRect.origin.x + aRect.size.width - 1, aRect.origin.y + aRect.size.height - 1);
    CGContextAddLineToPoint(context, aRect.origin.x, aRect.origin.y + aRect.size.height - 1);
    CGContextAddLineToPoint(context, aRect.origin.x, aRect.origin.y);
    CGContextClosePath(context);
    
    [[utils randomColor] setStroke];
    
    CGContextDrawPath(context,kCGPathStroke);
    CGContextRestoreGState(context);
}

#pragma mark - Public Methods

- (void)selectDay:(int)day
{
    int pre = self.firstOfPrev < 0 ? 0 : self.lastOfPrev - self.firstOfPrev + 1;
    
    int tot = day + pre;
    int row = tot / 7;
    int column = (tot % 7) - 1;
    
    self.selectedDay = day;
    self.selectedPortion = 1;
    
    if (day == self.today)
    {
        self.ImageViewSelected.image =[CalendarSettings sharedInstance].imageSelection;
        
        self.bMarkWasOnToday = YES;
    }
    else if (self.bMarkWasOnToday)
    {
        self.ImageViewSelected.image =[CalendarSettings sharedInstance].imageSelection;
        self.bMarkWasOnToday = NO;
    }
    
    [self addSubview:self.ImageViewSelected];
    
    if (column < 0)
    {
        column = 6;
        row--;
    }
    
    CGRect r = self.ImageViewSelected.frame;
    
    r.origin.x = column * self.width;
    r.origin.y = row * self.height;
    r.size.width = self.width;
    r.size.height = self.height;
    
    self.ImageViewSelected.frame = r;
}

- (NSDate *)dateSelected
{
    if (self.selectedDay < 1 || self.selectedPortion != 1)
        return nil;
    
    TKDateInformation info = [self.monthDate dateInformation];
    info.day = self.selectedDay;
    return [NSDate dateFromDateInformation:info];
}

- (void)setTarget:(id)t action:(SEL)a
{
    self.target = t;
    action = a;
}

#pragma mark - Public Static Methods

+ (NSArray *)rangeOfDatesInMonthGrid:(NSDate *)date startOnSunday:(BOOL)sunday
{
    NSDate *firstDate, *lastDate;
    
    TKDateInformation info = [date dateInformation];
    info.day = 1;
    NSDate *d = [NSDate dateFromDateInformation:info];
    info = [d dateInformation];
    
    if (info.weekday > 1)
    {
        TKDateInformation info2 = info;
        info2.month--;
        if (info2.month < 1)
        {
            info2.month = 12;
            info2.year--;
        }
        NSDate *previousMonth = [NSDate dateFromDateInformation:info2];
        int preDayCnt = [previousMonth daysInMonth];
        info2.day = preDayCnt - info.weekday + 2;
        firstDate = [NSDate dateFromDateInformation:info2];
    }
    else
    {
        firstDate = d;
    }
    
    int daysInMonth = [d daysInMonth];
    info.day = daysInMonth;
    NSDate *lastInMonth = [NSDate dateFromDateInformation:info];
    info = [lastInMonth dateInformation];
    if (info.weekday < 7)
    {
        info.day = 7 - info.weekday;
        info.month++;
        if (info.month > 12)
        {
            info.month = 1;
            info.year++;
        }
        lastDate = [NSDate dateFromDateInformation:info];
    }
    else
    {
        lastDate = lastInMonth;
    }
    
    return [NSArray arrayWithObjects:firstDate, lastDate, nil];
}



@end