//
//  CalendarConsts.h
//  Sidekick
//
//  Created by Ricol Wang on 28/05/13.
//
//

#ifndef Sidekick_CalendarConsts_h
#define Sidekick_CalendarConsts_h

#define kSun NSLocalizedString(@"Sun",@"Sun")
#define kMon NSLocalizedString(@"Mon",@"Mon")
#define kTue NSLocalizedString(@"Tue",@"Tue")
#define kWed NSLocalizedString(@"Wed",@"Wed")
#define kThu NSLocalizedString(@"Thu",@"Thu")
#define kFri NSLocalizedString(@"Fri",@"Fri")
#define kSat NSLocalizedString(@"Sat",@"Sat")

//#define _COLOR

#endif
