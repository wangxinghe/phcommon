//
//  CalendarMonthViewDelegate.h
//  Sidekick
//
//  Created by Ricol Wang on 28/05/13.
//
//

#import <Foundation/Foundation.h>

@class CalendarMonthView;

@protocol CalendarMonthViewDelegate <NSObject>

- (void) calendarMonthView:(CalendarMonthView*)monthView didSelectDate:(NSDate*)d;
- (void) calendarMonthView:(CalendarMonthView*)monthView monthDidChange:(NSDate*)d;

@end
