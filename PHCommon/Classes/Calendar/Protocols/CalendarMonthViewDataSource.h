//
//  CalendarMonthViewDataSource.h
//  Sidekick
//
//  Created by Ricol Wang on 28/05/13.
//
//

#import <Foundation/Foundation.h>

@class CalendarMonthView;

@protocol CalendarMonthViewDataSource <NSObject>

- (BOOL)calendarMonthView:(CalendarMonthView *)montheView markForDate:(NSDate *)date;

@end
