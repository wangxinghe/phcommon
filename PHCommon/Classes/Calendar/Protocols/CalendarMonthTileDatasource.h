//
//  CalendarMonthTileDatasource.h
//  PHCommon
//
//  Created by Ricol Wang on 30/07/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CalendarMonthTile;

@protocol CalendarMonthTileDatasource <NSObject>

- (BOOL)calendarMonthTile:(CalendarMonthTile *)monthTile markForDate:(NSDate *)date;

@end
