//
//  CalendarMonthTileDelegate.h
//  PHCommon
//
//  Created by Ricol Wang on 31/07/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CalendarMonthTile;

@protocol CalendarMonthTileDelegate <NSObject>

- (void) calendarMonthTile:(CalendarMonthTile*)monthTile didSelectDate:(NSDate*)d;

@end
