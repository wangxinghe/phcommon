//
//  CalendarMonthView+private.h
//  Sidekick
//
//  Created by Devin Ross Modified by Ricol Wang on 28/05/13.
//
//

#import "CalendarMonthView.h"

@interface CalendarMonthView (private)

- (NSDate*) firstOfMonthFromDate:(NSDate*)date;
- (NSDate*) nextMonthFromDate:(NSDate*)date;
- (NSDate*) previousMonthFromDate:(NSDate*)date;

@end
