//
//  CalendarMonthView+private.m
//  Sidekick
//
//  Created by Devin Ross Modified by Ricol Wang on 28/05/13.
//
//

#import "CalendarMonthView+private.h"
#import "NSDate+TKCategory.h"

@implementation CalendarMonthView (privateMeth)

- (NSDate*) firstOfMonthFromDate:(NSDate*)date
{
	TKDateInformation info = [date dateInformation];
	info.day = 1;
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	return [NSDate dateFromDateInformation:info];
}

- (NSDate*) nextMonthFromDate:(NSDate*)date
{
	TKDateInformation info = [date dateInformation];
	info.month++;
	if(info.month>12){
		info.month = 1;
		info.year++;
	}
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	
	return [NSDate dateFromDateInformation:info];
}

- (NSDate*) previousMonthFromDate:(NSDate*)date
{
	TKDateInformation info = [date dateInformation];
	info.month--;

	if(info.month<1){
		info.month = 12;
		info.year--;
	}
	
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	return [NSDate dateFromDateInformation:info];
}

@end
