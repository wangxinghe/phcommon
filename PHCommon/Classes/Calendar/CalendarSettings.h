//
//  CalendarSettings.h
//  PHCommon
//
//  Created by Ricol Wang on 3/08/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISingleton.h"

@interface CalendarSettings : NSObject <ISingleton>

@property (strong, nonatomic) NSBundle *theBundle;
@property (strong, nonatomic) UIColor *colorToday;
@property (strong, nonatomic) UIColor *colorDot;
@property (strong, nonatomic) UIColor *colorDate;
@property (strong, nonatomic) UIImage *imageSelection;
@property (strong, nonatomic) NSNumber *fontSizeDate;
@property (strong, nonatomic) NSNumber *fontSizeDot;
@property (strong, nonatomic) UIColor *colorPreviousDays;
@property (strong, nonatomic) UIColor *colorNextDays;

@end
