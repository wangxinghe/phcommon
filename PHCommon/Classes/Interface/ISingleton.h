//
//  ISingleton.h
//  WalkCoverMac
//
//  Created by Ricol Wang on 24/03/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ISingleton <NSObject>

+ (instancetype)sharedInstance;

@end
