//
//  PHBasicObjectWithObserver.h
//  WeTrak
//
//  Created by Ricol Wang on 20/10/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PHBasicObjectWithObserver : NSObject

@property (strong, nonatomic) NSMutableSet *mutableSetObservers;

- (void)addObserver:(id)object;
- (void)removeObserver:(id)object;

@end
