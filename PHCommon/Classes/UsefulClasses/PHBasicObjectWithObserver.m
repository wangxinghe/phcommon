//
//  PHBasicObjectWithObserver.m
//  WeTrak
//
//  Created by Ricol Wang on 20/10/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import "PHBasicObjectWithObserver.h"

@implementation PHBasicObjectWithObserver

#pragma mark - Life Cycle

- (void)dealloc
{
    [self.mutableSetObservers removeAllObjects];
}

#pragma mark - Reader Setter

- (NSMutableSet *)mutableSetObservers
{
    if (!_mutableSetObservers)
    {
        _mutableSetObservers = [NSMutableSet new];
    }
    
    return _mutableSetObservers;
}

#pragma mark - Public Methods

- (void)addObserver:(id)object
{
    if (object)
        [self.mutableSetObservers addObject:object];
}

- (void)removeObserver:(id)object
{
    if (object)
        [self.mutableSetObservers removeObject:object];
}

@end
