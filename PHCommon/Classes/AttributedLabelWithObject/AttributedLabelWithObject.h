//
//  AttributedLabelWithObject.h
//  PHCustomControls
//
//  Created by Ricol Wang on 25/09/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import "TTTAttributedLabel.h"

@interface AttributedLabelWithObject : TTTAttributedLabel

@property (strong) NSObject *object;
@property int tag;

@end
