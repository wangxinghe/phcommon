//
//  InforDialog.h
//
//  comments: customized by ricolwang based on "MBProgressHUD" class

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface InforDialog : MBProgressHUD

//public methods for displaying information dialog
+ (void)showInforWithStatus:(NSString *)string DisplayTime:(float)displayTime;
+ (void)showSuccessWithStatus:(NSString *)string DisplayTime:(float)displayTime;
+ (void)showErrorWithStatus:(NSString *)string DisplayTime:(float)displayTime;

@end
