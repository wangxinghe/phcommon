//
//  InforDialog.m
//

#import "InforDialog.h"
#import "utils.h"

//#define _DEBUG

#define WIDTH 50
#define HEIGHT 50

@interface InforDialogView : UIView

@property (strong) UIImageView *imageView;

@property (strong, nonatomic) UIImage *imageAttention;
@property (strong, nonatomic) UIImage *imageSuccess;
@property (strong, nonatomic) UIImage *imageError;

@end

@implementation InforDialogView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.imageView.contentMode = UIViewContentModeCenter;
        self.imageView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
#ifdef _DEBUG
        self.imageView.backgroundColor = [utils randomColor];
#else
        self.imageView.backgroundColor = [UIColor clearColor];
#endif
        
        [self addSubview:self.imageView];
    }
    
    return self;
}

#pragma mark - Override Methods

- (UIImage *)imageAttention
{
    if (!_imageAttention)
    {
        _imageAttention = [UIImage imageNamed:@"InforDialog.bundle/attention.png"];
    }
    
    return _imageAttention;
}

- (UIImage *)imageSuccess
{
    if (!_imageSuccess)
    {
        _imageSuccess = [UIImage imageNamed:@"InforDialog.bundle/success.png"];
    }
    
    return _imageSuccess;
}

- (UIImage *)imageError
{
    if (!_imageError)
    {
        _imageError = [UIImage imageNamed:@"InforDialog.bundle/error.png"];
    }
    
    return _imageError;
}

@end

@interface InforDialog () <MBProgressHUDDelegate>

@end

@implementation InforDialog

#pragma mark - MBProgressHUDDelegate

- (void)hudWasHidden:(MBProgressHUD *)hud
{
    [hud removeFromSuperview];
    hud = nil;
}

#pragma mark - Public Methods

+ (void)showInforWithStatus:(NSString *)string DisplayTime:(float)displayTime
{
    @try
    {
        UIWindow *tmpWindow = [[[UIApplication sharedApplication] delegate] window];
        InforDialog *tmpHUD = [InforDialog showHUDAddedTo:tmpWindow animated:YES];
        tmpHUD.mode = MBProgressHUDModeCustomView;
        tmpHUD.userInteractionEnabled = NO;
        tmpHUD.delegate = tmpHUD;
        InforDialogView *tmpView = [[InforDialogView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        tmpView.imageView.image = tmpView.imageAttention;
        tmpHUD.detailsLabelText = string;
        tmpHUD.customView = tmpView;
        
        [utils runAfter:displayTime andBlock:^(){
            [tmpHUD hide:YES];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception(InforDialog): Can't show InforDialog! - Message: %@", string);
    }
    @finally
    {
        ;
    }
}

+ (void)showSuccessWithStatus:(NSString *)string DisplayTime:(float)displayTime
{
    @try
    {
        UIWindow *tmpWindow = [[[UIApplication sharedApplication] delegate] window];
        InforDialog *tmpHUD = [InforDialog showHUDAddedTo:tmpWindow animated:YES];
        tmpHUD.mode = MBProgressHUDModeCustomView;
        tmpHUD.userInteractionEnabled = NO;
        tmpHUD.delegate = tmpHUD;
        InforDialogView *tmpView = [[InforDialogView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        tmpView.imageView.image = tmpView.imageSuccess;
        tmpHUD.detailsLabelText = string;
        tmpHUD.customView = tmpView;
        
        [utils runAfter:displayTime andBlock:^(){
            [tmpHUD hide:YES];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception(InforDialog): Can't show InforDialog! - Message: %@", string);
    }
    @finally
    {
        ;
    }
}

+ (void)showErrorWithStatus:(NSString *)string DisplayTime:(float)displayTime
{
    @try
    {
        UIWindow *tmpWindow = [[[UIApplication sharedApplication] delegate] window];
        InforDialog *tmpHUD = [InforDialog showHUDAddedTo:tmpWindow animated:YES];
        tmpHUD.mode = MBProgressHUDModeCustomView;
        tmpHUD.userInteractionEnabled = NO;
        tmpHUD.delegate = tmpHUD;
        InforDialogView *tmpView = [[InforDialogView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        tmpView.imageView.image = tmpView.imageError;
        tmpHUD.detailsLabelText = string;
        tmpHUD.customView = tmpView;
        
        [utils runAfter:displayTime andBlock:^(){
            [tmpHUD hide:YES];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception(InforDialog): Can't show InforDialog! - Message: %@", string);
    }
    @finally
    {
        ;
    }
}

@end
