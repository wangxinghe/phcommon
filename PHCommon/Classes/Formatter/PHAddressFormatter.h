//
//  PHAddressFormatter.h
//
//
//  Created by Ricol Wang on 15/12/2014
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PHAddressFormatter : NSObject

@property (copy) NSString *unitType;
@property (copy) NSString *unitNumber;
@property (copy) NSString *levelType;
@property (copy) NSString *levelNumber;
@property (copy) NSString *streetNumber;
@property (copy) NSString *streetName;
@property (copy) NSString *streetType;
@property (copy) NSString *streetSuffix;
@property (copy) NSString *suburb;
@property (copy) NSString *state;
@property (copy) NSString *country;
@property (copy) NSString *postcode;

- (NSString *)formattedAddressString;

@end
