//
//  PHAddressFormatter.m
//
//
//  Created by Ricol Wang on 15/12/2014
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "PHAddressFormatter.h"
#import "utils.h"

@implementation PHAddressFormatter

- (NSString *)formattedAddressString
{
    self.unitType = [utils trimBoth:self.unitType];
    self.unitType = self.unitType == nil ? @"" : self.unitType;
    self.unitNumber = [utils trimBoth:self.unitNumber];
    self.unitNumber = self.unitNumber == nil ? @"" : self.unitNumber;
    self.levelType = [utils trimBoth:self.levelType];
    self.levelType = self.levelType == nil ? @"" : self.levelType;
    self.levelNumber = [utils trimBoth:self.levelNumber];
    self.levelNumber = self.levelNumber == nil ? @"" : self.levelNumber;
    self.streetType = [utils trimBoth:self.streetType];
    self.streetType = self.streetType == nil ? @"" : self.streetType;
    self.streetSuffix = [utils trimBoth:self.streetSuffix];
    self.streetSuffix = self.streetSuffix == nil ? @"" : self.streetSuffix;
    self.streetName = self.streetName == nil ? @"" : self.streetName;
    self.streetNumber = self.streetNumber == nil ? @"" : self.streetNumber;
    self.suburb = self.suburb == nil ? @"" : self.suburb;
    self.postcode = self.postcode == nil ? @"" : self.postcode;
    self.state = self.state == nil ? @"" : self.state;
    self.country = self.country == nil ? @"" : self.country;
    
    NSMutableString *tmpMutableStrAddress = [[NSMutableString alloc] init];
    
    if (![self.unitType isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@ ", self.unitType];
    }
    
    if (![self.unitNumber isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@ ", self.unitNumber];
    }
    
    if (![self.levelType isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@ ", self.levelType];
    }
    
    if (![self.levelNumber isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@ ", self.levelNumber];
    }
    
    if (![self.streetNumber isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@ ", self.streetNumber];
    }
    
    if (![self.streetName isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@ ", self.streetName];
    }
    
    if (![self.streetSuffix isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@ ", self.streetSuffix];
    }
    
    if (![self.streetType isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@ ", self.streetType];
    }
    
    if (![self.suburb isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@ ", self.suburb];
    }
    
    if (![self.postcode isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@ ", self.postcode];
    }
    
    if (![self.state isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@ ", self.state];
    }
    
    if (![self.country isEqualToString:@""])
    {
        [tmpMutableStrAddress appendFormat:@"%@", self.country];
    }
    
    NSArray *tmpArray = [tmpMutableStrAddress componentsSeparatedByString:@" "];
    NSMutableArray *tmpMutableArray = [NSMutableArray new];
    
    for (uint i = 0; i < tmpArray.count; i++)
    {
        NSString *tmpStr = [tmpArray[i] copy];
        NSString *tmpStrNoSpace = [utils trimBoth:tmpStr];
        if ([tmpStrNoSpace isEqualToString:@""]) continue;
        [tmpMutableArray addObject:tmpStrNoSpace];
    }
    NSString *tmpStrAddress = [tmpMutableArray componentsJoinedByString:@" "];
    
    return tmpStrAddress;
}

@end
