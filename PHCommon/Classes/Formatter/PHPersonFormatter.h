//
//  PHPersonFormatter.h
//  WalkCover
//
//  Created by Ricol Wang on 11/02/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PHPersonFormatter : NSObject

@property (copy) NSString *strTitle;
@property (copy) NSString *strFirstName;
@property (copy) NSString *strLastName;

- (NSString *)formattedPersonString;

@end
