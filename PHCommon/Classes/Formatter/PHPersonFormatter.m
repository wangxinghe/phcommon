//
//  PHPersonFormatter.m
//  WalkCover
//
//  Created by Ricol Wang on 11/02/2014.
//  Copyright (c) 2014 Philology Pty. Ltd. All rights reserved.
//

#import "PHPersonFormatter.h"
#import "utils.h"

@implementation PHPersonFormatter

- (NSString *)formattedPersonString
{
    self.strTitle = self.strTitle == nil ? @"" : self.strTitle;
    self.strFirstName = self.strFirstName == nil ? @"" : self.strFirstName;
    self.strLastName = self.strLastName == nil ? @"" : self.strLastName;
    
    NSMutableString *tmpMutableStr = [[NSMutableString alloc] init];
    
    if (![self.strTitle isEqualToString:@""])
    {
        [tmpMutableStr appendFormat:@"%@ ", self.strTitle];
    }
    
    if (![self.strFirstName isEqualToString:@""])
    {
        [tmpMutableStr appendFormat:@"%@ ", self.strFirstName];
    }
    
    if (![self.strLastName isEqualToString:@""])
    {
        [tmpMutableStr appendFormat:@"%@ ", self.strLastName];
    }

    NSArray *tmpArray = [tmpMutableStr componentsSeparatedByString:@" "];
    NSMutableArray *tmpMutableArray = [NSMutableArray new];
    
    for (uint i = 0; i < tmpArray.count; i++)
    {
        NSString *tmpStr = [tmpArray[i] copy];
        NSString *tmpStrNoSpace = [utils trimBoth:tmpStr];
        if ([tmpStrNoSpace isEqualToString:@""]) continue;
        [tmpMutableArray addObject:tmpStrNoSpace];
    }
    NSString *tmpStr = [tmpMutableArray componentsJoinedByString:@" "];
    
//    NSLog(@"PersonFormatter->tmpStr: %@", tmpStr);
    
    return tmpStr;
}

@end
