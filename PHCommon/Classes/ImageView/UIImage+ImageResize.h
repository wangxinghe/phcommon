//
//  UIImage+ImageResize.h
//  BLS150
//
//  Created by Ricol Wang on 5/06/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageResize)

- (UIImage *)resizeImageWithnewSize:(CGSize)newSize;

@end
