//
//  UIImage+Negative.h
//  WeTrak
//
//  Created by Ricol Wang on 11/09/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Negative)

- (UIImage *)negativeImage;

@end
