//
//  ImageViewWithGesture.h
//  Sidekick
//
//  Created by Ricol Wang on 16/09/13.
//
//

#import <UIKit/UIKit.h>
#import "ImageViewWithGestureDelegate.h"

@interface ImageViewWithGesture : UIImageView

@property (weak) id <ImageViewWithGestureDelegate> delegate;

- (void)RegisterDoubleTapGesture:(BOOL)flag;
- (void)RegisterSingleTapGesture:(BOOL)flag;

@end
