//
//  ImageViewWithGesture.m
//  Sidekick
//
//  Created by Ricol Wang on 16/09/13.
//
//

#import "ImageViewWithGesture.h"

@interface ImageViewWithGesture ()

@property (strong, nonatomic) UITapGestureRecognizer *gestureSingleTap;
@property (strong, nonatomic) UITapGestureRecognizer *gestureDoubleTap;

@end

@implementation ImageViewWithGesture

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {

    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {

    }
    return self;
}

- (id)initWithImage:(UIImage *)image
{
    self = [super initWithImage:image];
    if (self)
    {

    }
    return self;
}

- (id)initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage
{
    self = [super initWithImage:image highlightedImage:highlightedImage];
    if (self)
    {

    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Public Methods

- (void)RegisterDoubleTapGesture:(BOOL)flag
{
    if (flag)
        [self addGestureRecognizer:self.gestureDoubleTap];
    else
        [self removeGestureRecognizer:self.gestureDoubleTap];
}

- (void)RegisterSingleTapGesture:(BOOL)flag
{
    if (flag)
        [self addGestureRecognizer:self.gestureSingleTap];
    else
        [self removeGestureRecognizer:self.gestureSingleTap];
}

#pragma mark - Override Methods

- (UITapGestureRecognizer *)gestureSingleTap
{
    if (!_gestureSingleTap)
    {
        _gestureSingleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(HandleSingleTapGesture:)];
        _gestureSingleTap.numberOfTapsRequired = 1;
        _gestureSingleTap.numberOfTouchesRequired = 1;
        [_gestureSingleTap requireGestureRecognizerToFail:self.gestureDoubleTap];
    }
    
    return _gestureSingleTap;
}

- (UITapGestureRecognizer *)gestureDoubleTap
{
    if (!_gestureDoubleTap)
    {
        _gestureDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(HandleDoubleTapGesture:)];
        _gestureDoubleTap.numberOfTapsRequired = 2;
        _gestureDoubleTap.numberOfTouchesRequired = 1;
    }
    
    return _gestureDoubleTap;
}

#pragma mark - Private Methods

- (void)HandleSingleTapGesture:(UITapGestureRecognizer *)tapGesture
{
    if ([self.delegate respondsToSelector:@selector(ImageViewWithGestureSingleTapped:andPosition:)])
    {
        CGPoint location = [tapGesture locationInView:self];
        [self.delegate ImageViewWithGestureSingleTapped:self andPosition:location];
    }
}

- (void)HandleDoubleTapGesture:(UITapGestureRecognizer *)tapGesture
{
    if ([self.delegate respondsToSelector:@selector(ImageViewWithGestureDoubleTapped:andPosition:)])
    {
        CGPoint location = [tapGesture locationInView:self];
        [self.delegate ImageViewWithGestureDoubleTapped:self andPosition:location];
    }
}

@end
