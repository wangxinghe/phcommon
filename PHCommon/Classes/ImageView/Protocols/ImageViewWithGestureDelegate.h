//
//  ImageViewWithGestureDelegate.h
//  PHCustomControls
//
//  Created by Ricol Wang on 17/09/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ImageViewWithGesture;

@protocol ImageViewWithGestureDelegate <NSObject>

@optional

- (void)ImageViewWithGestureSingleTapped:(ImageViewWithGesture *)imageView andPosition:(CGPoint)position;
- (void)ImageViewWithGestureDoubleTapped:(ImageViewWithGesture *)imageView andPosition:(CGPoint)position;
- (void)ImageViewWithGestureLongPressed:(ImageViewWithGesture *)imageView andPosition:(CGPoint)position;

@end
