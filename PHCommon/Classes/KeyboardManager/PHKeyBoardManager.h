//
//  PHKeyBoardManager.h
//  Sidekick
//
//  Created by Gaurav Goyal and Ricol Wang on 16/04/13.
//
//

#import <UIKit/UIKit.h>

@interface PHKeyBoardManager : NSObject

- (void)Enable:(BOOL)enable;

+ (id)sharedKeyBoardManager;

@end
