//
//  PHKeyBoardManager.m
//  Sidekick
//
//  Created by Gaurav Goyal and Ricol Wang on 16/04/13.
//
//

#import "PHKeyBoardManager.h"
#import "utils.h"

#define HEIGHT_TOOLBAR 50

@interface PHKeyBoardManager ()

{
    CGFloat animationDuration;
}

@property (strong, nonatomic) UIView *activeView;
@property (strong) NSNotification *notification;

@end

@implementation PHKeyBoardManager

#pragma mark - View Life Cycle

+ (id)sharedKeyBoardManager
{
    static PHKeyBoardManager *TheKeyBoardManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        TheKeyBoardManager = [[self alloc] init];
    });
    return TheKeyBoardManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        animationDuration = 0.25;
    }
    return self;
}

#pragma mark - Override Methods

- (void)setActiveView:(UIView *)activeView
{
    if (activeView == nil)
        [self resetFrame];
    
    _activeView = activeView;
}

- (void)Enable:(BOOL)enable
{
    static int stackLevel = 0;
    if (enable)
    {
        stackLevel++;
        if (stackLevel > 1) return;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidBeginEditing:) name:UITextFieldTextDidBeginEditingNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidEndEditing:) name:UITextFieldTextDidEndEditingNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidBeginEditing:) name:UITextViewTextDidBeginEditingNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewdDidEndEditing:) name:UITextViewTextDidEndEditingNotification object:nil];
    }else
    {
        stackLevel--;
        if (stackLevel > 0) return;
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidBeginEditingNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidEndEditingNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidBeginEditingNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidEndEditingNotification object:nil];
    }
}

#pragma mark - Helper Animation function

-(void)setRootViewFrame:(CGRect)frame
{
    NSLog(@"setRootViewFrame...(%.1f, %.1f, %.1f, %.1f", frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
    [UIView animateWithDuration:animationDuration animations:^{
        [self.activeView.window.rootViewController.view setFrame:frame];
    }];
}

#pragma mark - Device Orientation Supported logic
//Setting rootViewController's frame according to deviceOrientation.
-(void)handleLandscapeRightWithKeyboardNotification:(NSNotification*)aNotification
{
    NSLog(@"handleLandscapeRightWithKeyboardNotification...");
    if ([utils isIOS8])
    {
        //Getting UserInfo.
        NSDictionary* info = [aNotification userInfo];
        
        //Getting UIKeyboardSize.
        CGRect rectKeyboard = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        rectKeyboard.origin.y -= HEIGHT_TOOLBAR;
        rectKeyboard.size.height += HEIGHT_TOOLBAR;
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        
        CGRect textFieldViewRect = [self.activeView.superview convertRect:self.activeView.frame toView:window];
        
        CGRect rootViewRect = window.rootViewController.view.frame;
        
        if (CGRectGetMinY(rectKeyboard) < CGRectGetMaxY(textFieldViewRect))
        {
            float move = CGRectGetMinY(rectKeyboard) - CGRectGetMinY(textFieldViewRect);
            NSLog(@"move: %f", move);
            rootViewRect.origin.y += move;
            [self setRootViewFrame:rootViewRect];
        }
    }else
    {
        //Getting UserInfo.
        NSDictionary* info = [aNotification userInfo];
        
        //Getting UIKeyboardSize.
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        
        //Adding 10 more height to keyboard.
        kbSize.width += 10;
        
        //Getting KeyWindow object.
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        
        //Converting Rectangle according to window bounds.
        CGRect textFieldViewRect = [self.activeView.superview convertRect:self.activeView.frame toView:window];
        //Getting RootViewRect.
        CGRect rootViewRect = window.rootViewController.view.frame;
        //Getting Application Frame.
        CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
        
        //Calculate movement which should be scrolled.
        float move = kbSize.width-CGRectGetMinX(textFieldViewRect);
        
        //If textField is showing on screen(i.e. y origin is positive.
        if (CGRectGetMaxX(textFieldViewRect)<=CGRectGetWidth(appFrame))
        {
            //If move is positive, Then Keyboard must be hiding our textField object.
            if(move >= 0)
            {
                //Adjusting rootView controller frame.
                rootViewRect.origin.x += move;
                [self setRootViewFrame:rootViewRect];
            }
            //Else if our rootViewController frame's y is negative(Disturbed).
            else if(CGRectGetMinX(rootViewRect)>CGRectGetMinX(appFrame))
            {
                //Calculating disturbed distance.
                CGFloat disturbDistance = CGRectGetMinX(appFrame)-CGRectGetMinX(rootViewRect);
                
                //Adding movement or disturbed distance to origin.(Don't be trapped on this calculation)
                rootViewRect.origin.x += move>disturbDistance?move:disturbDistance;
                
                if (CGRectGetMinX(rootViewRect) == CGRectGetMinX(appFrame))
                    [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
                else
                    [self setRootViewFrame:rootViewRect];
            }
        }
        //Else if our rootViewController frame's y is negative(Disturbed).
        else if(CGRectGetMinX(rootViewRect)>CGRectGetMinX(appFrame))
        {
            //Calculating disturbed distance.
            CGFloat disturbDistance = CGRectGetMinX(appFrame)-CGRectGetMinX(rootViewRect);
            
            //Adding movement or disturbed distance to origin.(Don't be trapped on this calculation)
            rootViewRect.origin.x += move>disturbDistance?move:disturbDistance;
            
            if (CGRectGetMinX(rootViewRect) == CGRectGetMinX(appFrame))
                [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
            else
                [self setRootViewFrame:rootViewRect];
        }
    }
}

-(void)handleLandscapeLeftWithKeyboardNotification:(NSNotification*)aNotification
{
    NSLog(@"handleLandscapeLeftWithKeyboardNotification...");
    if ([utils isIOS8])
    {
        //Getting UserInfo.
        NSDictionary* info = [aNotification userInfo];
        
        //Getting UIKeyboardSize.
        CGRect rectKeyboard = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        rectKeyboard.origin.y -= HEIGHT_TOOLBAR;
        rectKeyboard.size.height += HEIGHT_TOOLBAR;
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        
        CGRect textFieldViewRect = [self.activeView.superview convertRect:self.activeView.frame toView:window];
        
        CGRect rootViewRect = window.rootViewController.view.frame;
        
        if (CGRectGetMinY(rectKeyboard) < CGRectGetMaxY(textFieldViewRect))
        {
            float move = CGRectGetMinY(rectKeyboard) - CGRectGetMinY(textFieldViewRect);
            NSLog(@"move: %f", move);
            rootViewRect.origin.y += move;
            [self setRootViewFrame:rootViewRect];
        }
    }else
    {
        //Getting UserInfo.
        NSDictionary* info = [aNotification userInfo];
        
        //Getting UIKeyboardSize.
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        
        //Adding 10 more height to keyboard.
        kbSize.width += 10;
        
        //Getting KeyWindow object.
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        
        //Converting Rectangle according to window bounds.
        CGRect textFieldViewRect = [self.activeView.superview convertRect:self.activeView.frame toView:window];
        //Getting RootViewRect.
        CGRect rootViewRect = window.rootViewController.view.frame;
        //Getting Application Frame.
        CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
        
        //Calculate movement which should be scrolled.
        float move = CGRectGetMaxX(textFieldViewRect)-(CGRectGetWidth(window.frame)-kbSize.width);
        
        //If textField is showing on screen(i.e. y origin is positive.
        if (CGRectGetMinX(textFieldViewRect)>=CGRectGetMinX(appFrame))
        {
            //If move is positive, Then Keyboard must be hiding our textField object.
            if(move >= 0)
            {
                //Adjusting rootView controller frame.
                rootViewRect.origin.x -= move;
                [self setRootViewFrame:rootViewRect];
            }
            //Else if our rootViewController frame's y is negative(Disturbed).
            else if(CGRectGetMinX(rootViewRect)<CGRectGetMinX(appFrame))
            {
                //Calculating disturbed distance.
                CGFloat disturbDistance = CGRectGetMinX(rootViewRect)-CGRectGetMinX(appFrame);
                
                //Adding movement or disturbed distance to origin.(Don't be trapped on this calculation)
                rootViewRect.origin.x -= move>disturbDistance?move:disturbDistance;
                
                if (CGRectGetMinX(rootViewRect) == CGRectGetMinX(appFrame))
                    [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
                else
                    [self setRootViewFrame:rootViewRect];
            }
        }
        //Else if our rootViewController frame's y is negative(Disturbed).
        else if(CGRectGetMinX(rootViewRect)<CGRectGetMinX(appFrame))
        {
            //Calculating disturbed distance.
            CGFloat disturbDistance = CGRectGetMinX(rootViewRect)-CGRectGetMinX(appFrame);
            
            //Adding movement or disturbed distance to origin.(Don't be trapped on this calculation)
            rootViewRect.origin.x -= move>disturbDistance?move:disturbDistance;
            
            if (CGRectGetMinX(rootViewRect) == CGRectGetMinX(appFrame))
                [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
            else
                [self setRootViewFrame:rootViewRect];
        }
    }
}

-(void)handlePortraitWithKeyboardNotification:(NSNotification*)aNotification
{
    NSLog(@"handlePortraitWithKeyboardNotification...");
    if ([utils isIOS8])
    {
        //Getting UserInfo.
        NSDictionary* info = [aNotification userInfo];
        
        //Getting UIKeyboardSize.
        CGRect rectKeyboard = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        rectKeyboard.origin.y -= HEIGHT_TOOLBAR;
        rectKeyboard.size.height += HEIGHT_TOOLBAR;
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        
        CGRect textFieldViewRect = [self.activeView.superview convertRect:self.activeView.frame toView:window];
        
        CGRect rootViewRect = window.rootViewController.view.frame;
        
        if (CGRectGetMinY(rectKeyboard) < CGRectGetMaxY(textFieldViewRect))
        {
            float move = CGRectGetMinY(rectKeyboard) - CGRectGetMinY(textFieldViewRect);
            NSLog(@"move: %f", move);
            rootViewRect.origin.y += move;
            [self setRootViewFrame:rootViewRect];
        }
    }else
    {
        //Getting UserInfo.
        NSDictionary* info = [aNotification userInfo];
        
        //Getting UIKeyboardSize.
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        
        //Adding 10 more height to keyboard.
        kbSize.height += 10;
        
        //Getting KeyWindow object.
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        
        //Converting Rectangle according to window bounds.
        CGRect textFieldViewRect = [self.activeView.superview convertRect:self.activeView.frame toView:window];
        //Getting RootViewRect.
        CGRect rootViewRect = window.rootViewController.view.frame;
        //Getting Application Frame.
        CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
        
        //Calculate movement which should be scrolled.
        float move = CGRectGetMaxY(textFieldViewRect)-(CGRectGetHeight(window.frame)-kbSize.height);
        
        //If textField is showing on screen(i.e. y origin is positive.
        if (CGRectGetMinY(textFieldViewRect)>=CGRectGetMinY(appFrame))
        {
            //If move is positive, Then Keyboard must be hiding our textField object.
            if(move >= 0)
            {
                //Adjusting rootView controller frame.
                rootViewRect.origin.y -= move;
                [self setRootViewFrame:rootViewRect];
            }
            //Else if our rootViewController frame's y is negative(Disturbed).
            else if(CGRectGetMinY(rootViewRect)<CGRectGetMinY(appFrame))
            {
                //Calculating disturbed distance.
                CGFloat disturbDistance = CGRectGetMinY(rootViewRect)-CGRectGetMinY(appFrame);
                
                //Adding movement or disturbed distance to origin.(Don't be trapped on this calculation)
                rootViewRect.origin.y -= move>disturbDistance?move:disturbDistance;
                
                if (CGRectGetMinY(rootViewRect) == CGRectGetMinY(appFrame))
                    [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
                else
                    [self setRootViewFrame:rootViewRect];
            }
        }
        //Else if our rootViewController frame's y is negative(Disturbed).
        else if(CGRectGetMinY(rootViewRect)<CGRectGetMinY(appFrame))
        {
            //Calculating disturbed distance.
            CGFloat disturbDistance = CGRectGetMinY(rootViewRect)-CGRectGetMinY(appFrame);
            
            //Adding movement or disturbed distance to origin.(Don't be trapped on this calculation)
            rootViewRect.origin.y -= move>disturbDistance?move:disturbDistance;
            
            if (CGRectGetMinY(rootViewRect) == CGRectGetMinY(appFrame))
                [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
            else
                [self setRootViewFrame:rootViewRect];
        }
    }
}

-(void)handlePortraitUpsideDownWithKeyboardNotification:(NSNotification*)aNotification
{
    NSLog(@"handlePortraitUpsideDownWithKeyboardNotification...");
    if ([utils isIOS8])
    {
        //Getting UserInfo.
        NSDictionary* info = [aNotification userInfo];
        
        //Getting UIKeyboardSize.
        CGRect rectKeyboard = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        rectKeyboard.origin.y -= HEIGHT_TOOLBAR;
        rectKeyboard.size.height += HEIGHT_TOOLBAR;

        UIWindow *window = [[UIApplication sharedApplication] keyWindow];

        CGRect textFieldViewRect = [self.activeView.superview convertRect:self.activeView.frame toView:window];

        CGRect rootViewRect = window.rootViewController.view.frame;
        
        if (CGRectGetMinY(rectKeyboard) < CGRectGetMaxY(textFieldViewRect))
        {
            float move = CGRectGetMinY(rectKeyboard) - CGRectGetMinY(textFieldViewRect);
            NSLog(@"move: %f", move);
            rootViewRect.origin.y += move;
            [self setRootViewFrame:rootViewRect];
        }
    }else
    {
        //Getting UserInfo.
        NSDictionary* info = [aNotification userInfo];
        
        //Getting UIKeyboardSize.
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        
        //Adding 10 more height to keyboard.
        kbSize.height += 10;
        
        //Getting KeyWindow object.
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        
        //Converting Rectangle according to window bounds.
        CGRect textFieldViewRect = [self.activeView.superview convertRect:self.activeView.frame toView:window];
        //Getting RootViewRect.
        CGRect rootViewRect = window.rootViewController.view.frame;
        //Getting Application Frame.
        CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
        
        //Calculate movement which should be scrolled.
        float move = kbSize.height-CGRectGetMinY(textFieldViewRect);
        
        //If textField is showing on screen(i.e. y origin is positive.
        if (CGRectGetMaxY(textFieldViewRect)<=CGRectGetHeight(appFrame))
        {
            //If move is positive, Then Keyboard must be hiding our textField object.
            if(move >= 0)
            {
                //Adjusting rootView controller frame.
                rootViewRect.origin.y += move;
                [self setRootViewFrame:rootViewRect];
            }
            //Else if our rootViewController frame's y is negative(Disturbed).
            else if(CGRectGetMinY(rootViewRect)>CGRectGetMinY(appFrame))
            {
                //Calculating disturbed distance.
                CGFloat disturbDistance = CGRectGetMinY(appFrame)-CGRectGetMinY(rootViewRect);
                
                //Adding movement or disturbed distance to origin.(Don't be trapped on this calculation)
                rootViewRect.origin.y += move>disturbDistance?move:disturbDistance;
                
                if (CGRectGetMinY(rootViewRect) == CGRectGetMinY(appFrame))
                    [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
                else
                    [self setRootViewFrame:rootViewRect];
            }
        }
        //Else if our rootViewController frame's y is negative(Disturbed).
        else if(CGRectGetMinY(rootViewRect)>CGRectGetMinY(appFrame))
        {
            //Calculating disturbed distance.
            CGFloat disturbDistance = CGRectGetMinY(appFrame)-CGRectGetMinY(rootViewRect);
            
            //Adding movement or disturbed distance to origin.(Don't be trapped on this calculation)
            rootViewRect.origin.y += move>disturbDistance?move:disturbDistance;
            
            if (CGRectGetMinY(rootViewRect) == CGRectGetMinY(appFrame))
                [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
            else
                [self setRootViewFrame:rootViewRect];
        }
    }
}

#pragma mark - UIKeyboad Delegate methods
// Keyboard Will hide. So setting rootViewController to it's default frame.
- (void)keyboardWillHide:(NSNotification*)aNotification
{
    NSLog(@"keyboardWillHide...");
    CGFloat aDuration = [[aNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    if (aDuration!= 0.0f)
    {
        animationDuration = [[aNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    }
    
    //Setting rootViewController frame to it's original position.
    
    if ([utils isIOS8])
    {
        [self setRootViewFrame:[[UIScreen mainScreen] bounds]];
    }else
    {
        [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
    }
}

- (void)keyboardDidHide:(NSNotification*)aNotification
{
    NSLog(@"keyboardDidHide...");
    self.notification = nil;
}

//UIKeyboard Did shown. Adjusting RootViewController's frame according to device orientation.
-(void)keyboardWillShow:(NSNotification*)aNotification
{
    NSLog(@"keyboardWillShow...");
    self.notification = aNotification;
    
    CGFloat aDuration = [[aNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    if (aDuration!= 0.0f)
    {
        animationDuration = [[aNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    }

    switch ([[UIApplication sharedApplication] statusBarOrientation])
    {
        case UIInterfaceOrientationLandscapeLeft:
            [self handleLandscapeLeftWithKeyboardNotification:aNotification];
            break;
        case UIInterfaceOrientationLandscapeRight:
            [self handleLandscapeRightWithKeyboardNotification:aNotification];
            break;
        case UIInterfaceOrientationPortrait:
            [self handlePortraitWithKeyboardNotification:aNotification];
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            [self handlePortraitUpsideDownWithKeyboardNotification:aNotification];
            break;
        default:
            break;
    }
}

- (void)keyboardDidShow:(NSNotification*)aNotification
{
    NSLog(@"keyboardDidShow...");
    self.notification = aNotification;
}

#pragma mark - UITextField Delegate methods
//Fetching UITextField object from notification.
-(void)textFieldDidBeginEditing:(NSNotification*)notification
{
    NSLog(@"textFieldDidBeginEditing...");
    self.activeView = notification.object;
    
    if (self.notification)
    {
        CGFloat aDuration = [[self.notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        if (aDuration!= 0.0f)
        {
            animationDuration = [[self.notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        }
        
        switch ([[UIApplication sharedApplication] statusBarOrientation])
        {
            case UIInterfaceOrientationLandscapeLeft:
                [self handleLandscapeLeftWithKeyboardNotification:self.notification];
                break;
            case UIInterfaceOrientationLandscapeRight:
                [self handleLandscapeRightWithKeyboardNotification:self.notification];
                break;
            case UIInterfaceOrientationPortrait:
                [self handlePortraitWithKeyboardNotification:self.notification];
                break;
            case UIInterfaceOrientationPortraitUpsideDown:
                [self handlePortraitUpsideDownWithKeyboardNotification:self.notification];
                break;
            default:
                break;
        }
    }else
    {
        if ([utils isIOS8])
        {
            [self setRootViewFrame:[[UIScreen mainScreen] bounds]];
        }else
        {
            [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
        }
    }
}

//Removing fetched object.
-(void)textFieldDidEndEditing:(NSNotification*)notification
{
    NSLog(@"textFieldDidEndEditing...");
    self.activeView = nil;
}

#pragma mark - UITextView Delegate methods
//Fetching UITextView object from notification.
-(void)textViewDidBeginEditing:(NSNotification*)notification
{
    NSLog(@"textViewDidBeginEditing...");
    self.activeView = notification.object;
    
    if (self.notification)
    {
        CGFloat aDuration = [[self.notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        if (aDuration!= 0.0f)
        {
            animationDuration = [[self.notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        }
        
        switch ([[UIApplication sharedApplication] statusBarOrientation])
        {
            case UIInterfaceOrientationLandscapeLeft:
                [self handleLandscapeLeftWithKeyboardNotification:self.notification];
                break;
            case UIInterfaceOrientationLandscapeRight:
                [self handleLandscapeRightWithKeyboardNotification:self.notification];
                break;
            case UIInterfaceOrientationPortrait:
                [self handlePortraitWithKeyboardNotification:self.notification];
                break;
            case UIInterfaceOrientationPortraitUpsideDown:
                [self handlePortraitUpsideDownWithKeyboardNotification:self.notification];
                break;
            default:
                break;
        }
    }else
    {
        if ([utils isIOS8])
        {
            [self setRootViewFrame:[[UIScreen mainScreen] bounds]];
        }else
        {
            [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
        }
    }
}

//Removing fetched object.
-(void)textViewdDidEndEditing:(NSNotification*)notification
{
    NSLog(@"textViewdDidEndEditing...");
    self.activeView = nil;
}

#pragma mark - Private Methods

- (void)resetFrame
{
    NSLog(@"resetFrame...");
    if ([utils isIOS8])
    {
        [self setRootViewFrame:[[UIScreen mainScreen] bounds]];
    }else
    {
        [self setRootViewFrame:[[UIScreen mainScreen] applicationFrame]];
    }
}

@end
