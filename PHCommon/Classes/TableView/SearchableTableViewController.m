//
//  SearchableTableViewController.m
//  WalkCover
//
//  Created by ricolwang on 21/11/12.
//  Copyright (c) 2012 Philology Pty. Ltd. All rights reserved.
//

#import "SearchableTableViewController.h"
#import "utils.h"

#define SEARCHBAR_HEIGHT 40
#define BUTTON_WIDTH 120
#define BUTTON_Y 3
//#define _FOOT_IMAGE_BACKGROUND
//#define _DEBUG

#define String(s) NSLocalizedStringFromTable(s, @"PHCommon", nil)

@interface SearchableTableViewController ()

{
    CGRect mainFrame;
    BOOL bFirstTime;
}

@property (strong, nonatomic) NSArray *array_data;
@property (strong, nonatomic) NSMutableArray *array_filterdData;
@property (weak, nonatomic) id <SearchableTableViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *view_head;
@property (weak, nonatomic) IBOutlet UIView *view_body;
@property (weak, nonatomic) IBOutlet UIView *view_foot;
@property (weak, nonatomic) IBOutlet UIButton *btnEmptyValue;

- (IBAction)btnEmptyValueOnTapped:(id)sender;

@end

@implementation SearchableTableViewController

- (id) initWithDelegate:(id <SearchableTableViewDelegate>)delegate andData:(NSArray *)data withFrame:(CGRect)frame
{
    self = [super initWithNibName:@"SearchableTableViewController" bundle:nil];
    
    if (self)
    {
        self.delegate = delegate;
        self.array_data = [NSArray arrayWithArray:data];
        self.array_filterdData = [NSMutableArray arrayWithArray:data];
        mainFrame = frame;
        bFirstTime = YES;
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (bFirstTime)
    {
        self.view.frame = mainFrame;      
#ifdef _DEBUG
        self.searchBar.backgroundColor = [utils randomColor];
        self.tableView.backgroundColor = [utils randomColor];
        self.btnEmptyValue.backgroundColor = [utils randomColor];
        self.view_foot.backgroundColor = [utils randomColor];
        self.view_head.backgroundColor = [utils randomColor];
        self.view_body.backgroundColor = [utils randomColor];
        self.view.backgroundColor = [UIColor redColor];
#else
        self.searchBar.backgroundColor = [UIColor clearColor];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.view_head.backgroundColor = [UIColor clearColor];
        self.view_body.backgroundColor = [UIColor clearColor];
        self.view.backgroundColor = [UIColor whiteColor];
        self.searchBar.placeholder = String(@"SearchContent");
        self.view_foot.backgroundColor= [UIColor colorWithRed:210.0 / 255 green:210.0 / 255 blue:210.0 / 255 alpha:1];
#endif
        
        [self.btnEmptyValue setBackgroundImage:[UIImage imageNamed:@"blank-large-internal-button-orange-default"] forState:UIControlStateNormal];
        [self.btnEmptyValue setTitle:String(@"Clear") forState:UIControlStateNormal];
        self.btnEmptyValue.backgroundColor = [UIColor clearColor];
        [self.btnEmptyValue setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        bFirstTime = NO;
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (NSInteger)self.array_filterdData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierDefault = @"UITableViewCellDefault";
    static NSString *CellIdentifierSubtitle = @"UITableViewCellSubtitle";
    
    int row = indexPath.row;
    NSObject *tmpObject = self.array_filterdData[row];
    
    UITableViewCell *cell;
    
    if ([tmpObject isKindOfClass:[NSString class]])
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierDefault];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierDefault];
        NSString *tmpStrValue = (NSString *)tmpObject;
        cell.textLabel.text = tmpStrValue;
        cell.textLabel.numberOfLines = 2;
    }else if ([tmpObject isKindOfClass:[NSDictionary class]])
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierSubtitle];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifierSubtitle];
        NSDictionary *tmpDictValue = (NSDictionary *)tmpObject;
        cell.textLabel.text = tmpDictValue[KEY_TITLE];
        cell.textLabel.numberOfLines = 2;
        cell.detailTextLabel.text = tmpDictValue[KEY_SUBTITLE];
        cell.detailTextLabel.numberOfLines = 2;
        if (tmpDictValue[KEY_TEXTCOLOR])
        {
            tmpObject = tmpDictValue[KEY_TEXTCOLOR];
            if ([tmpObject isKindOfClass:[UIColor class]])
            {
                cell.textLabel.textColor = (UIColor *)tmpObject;
                cell.detailTextLabel.textColor = (UIColor *)tmpObject;
            }
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate searchableTableView:self didSelectRowAtIndexPath:indexPath FromData:self.array_filterdData];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.array_filterdData removeAllObjects];
    
    NSArray *tmpArray = nil;
    if (self.datasoure)
        tmpArray = [self.datasoure searchableTableViewFilterArray:self.array_data withSearchText:searchText andController:self];
    else
        tmpArray = [SearchableTableViewController filterArray:self.array_data withSearchText:searchText];
    
    self.array_filterdData = [NSMutableArray arrayWithArray:tmpArray];

    [self.tableView reloadData];
}

#pragma mark - filter method

+ (NSArray *)filterArray:(NSArray *)array withSearchText:(NSString *)text
{
    NSString *tmpText = nil;
    if (text)
    {
        tmpText = [NSString stringWithFormat:@"%@", text];
        tmpText = [tmpText uppercaseString];
    }

    NSArray *tmpArray = [NSArray arrayWithArray:array];
    NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] init];
    
    if (tmpText && (![tmpText isEqualToString:@""]))
    {
        for (NSUInteger i = 0; i < tmpArray.count; i++)
        {
            NSObject *tmpObject = tmpArray[i];
            
            if ([tmpObject isKindOfClass:[NSString class]])
            {
                NSString *tmpString = tmpArray[i];
                NSString *tmpUpperCaseString = [tmpString uppercaseString];
                if ([tmpUpperCaseString rangeOfString:tmpText].location == NSNotFound)
                    continue;
                else
                    [tmpMutableArray addObject:tmpObject];
            }else if ([tmpObject isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *tmpDict = tmpArray[i];
                NSString *tmpStringTitle = tmpDict[KEY_TITLE];
                NSString *tmpStringDescription = tmpDict[KEY_SUBTITLE];
                NSString *tmpStringTitleUppercase = [tmpStringTitle uppercaseString];
                NSString *tmpStringDescriptionUppercase = [tmpStringDescription uppercaseString];
                
                BOOL bFound = NO;
                
                if (tmpStringTitleUppercase)
                {
                    if ([tmpStringTitleUppercase rangeOfString:tmpText].location != NSNotFound) bFound = YES;
                }
                
                if (tmpStringDescriptionUppercase)
                {
                    if ([tmpStringDescriptionUppercase rangeOfString:tmpText].location != NSNotFound) bFound = YES;
                }
                
                if (bFound)
                    [tmpMutableArray addObject:tmpObject];
            }else
                continue;
        }
    }else
        [tmpMutableArray addObjectsFromArray:tmpArray];
    
    return tmpMutableArray;
}

#pragma mark - IBAction Methods

- (IBAction)btnEmptyValueOnTapped:(id)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.delegate searchableTableView:self didSelectRowAtIndexPath:indexPath FromData:@[@""]];
}

@end
