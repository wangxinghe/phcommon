//
//  SearchableTableViewDatasource.h
//  PHCommon
//
//  Created by Ricol Wang on 18/08/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SearchableTableViewDatasource <NSObject>

- (NSArray *)searchableTableViewFilterArray:(NSArray *)array withSearchText:(NSString *)text andController:(SearchableTableViewController *)vc;

@end
