//
//  SearchableTableViewController.h
//  WalkCover
//
//  Created by ricolwang on 21/11/12.
//  Copyright (c) 2012 Philology Pty. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchableTableViewDelegate.h"
#import "SearchableTableViewDatasource.h"

#define KEY_TITLE @"keyTitle"
#define KEY_SUBTITLE @"keySubtitle"
#define KEY_TEXTCOLOR @"keyTextColor"

@interface SearchableTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

- (id) initWithDelegate:(id <SearchableTableViewDelegate>)delegate andData:(NSArray *)data withFrame:(CGRect)frame;

@property (weak, nonatomic) id <SearchableTableViewDatasource> datasoure;
@property int tag;
@property (strong) NSObject *object;

+ (NSArray *)filterArray:(NSArray *)array withSearchText:(NSString *)text;

@end
