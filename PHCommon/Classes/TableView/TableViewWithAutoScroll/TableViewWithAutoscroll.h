//
//  TableViewWithAutoscroll.h
//  Cadeanco
//
//  Created by Ricol Wang on 7/07/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>

#define KEY_TITLE_TABLEVIEWWITHAUTOSCROLL_DATA @"KEY_TITLE_TABLEVIEWWITHAUTOSCROLL_DATA"
#define KEY_IDENTIFIER_TABLEVIEWWITHAUTOSCROLL_DATA @"KEY_IDENTIFIER_TABLEVIEWWITHAUTOSCROLL_DATA"

@interface TableViewWithAutoscroll : UITableView

@property (strong, nonatomic) NSNumber *capacity;

- (void)addItem:(NSDictionary *)dict;
- (void)removeItem:(NSDictionary *)dict;
- (void)clear;

@end
