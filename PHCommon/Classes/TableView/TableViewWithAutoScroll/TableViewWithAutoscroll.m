//
//  TableViewWithAutoscroll.m
//  Cadeanco
//
//  Created by Ricol Wang on 7/07/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import "TableViewWithAutoscroll.h"

#define IDENTIFIER_TABLEVIEWCELL @"standarduitableviewcell"

@interface TableViewWithAutoscroll () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *data;

@end

@implementation TableViewWithAutoscroll

@synthesize capacity = _capacity;

- (instancetype)init
{
    self = [super init];
    [self initData];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    [self initData];
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self initData];
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:style];
    [self initData];
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initData];
}

#pragma mark - Getter

- (NSMutableArray *)data
{
    if (!_data)
        _data = [NSMutableArray new];
    
    return _data;
}

- (NSNumber *)capacity
{
    if (!_capacity)
        _capacity = @(10000);
    return _capacity;
}

- (void)setCapacity:(NSNumber *)capacity
{
    if ([capacity intValue] < 1 || [capacity intValue] > 10000) return;
    else _capacity = capacity;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *tmpCell = [self dequeueReusableCellWithIdentifier:IDENTIFIER_TABLEVIEWCELL];
    if (!tmpCell)
    {
        tmpCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:IDENTIFIER_TABLEVIEWCELL];
    }
    
    NSDictionary *tmpDict = self.data[indexPath.row];
    tmpCell.textLabel.text = tmpDict[KEY_TITLE_TABLEVIEWWITHAUTOSCROLL_DATA];
    
    return tmpCell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Private Methods

- (void)initData
{
    self.dataSource = self;
    self.delegate = self;
}

#pragma mark - Public Methods

- (void)addItem:(NSDictionary *)dict
{
    [self beginUpdates];
    if (self.data.count >= self.capacity.intValue)
    {
        [self.data removeObjectAtIndex:0];
        [self deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    [self.data addObject:dict];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.data.count - 1 inSection:0];
    [self insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
    [self endUpdates];
    
    [self scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)removeItem:(NSString *)identifier
{
    int target = -1;
    for (int i = 0; i < self.data.count; i++)
    {
        NSDictionary *dict = self.data[i];
        if ([dict[KEY_IDENTIFIER_TABLEVIEWWITHAUTOSCROLL_DATA] isEqualToString:identifier])
        {
            target = i;
            break;
        }
    }
    
    if (target > -1)
    {
        [self beginUpdates];
        [self.data removeObjectAtIndex:target];
        [self deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:target inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self endUpdates];
    }
}

- (void)clear
{
    [self beginUpdates];

    NSMutableArray *ma = [NSMutableArray new];
    
    for (int i = 0; i < self.data.count; i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [ma addObject:indexPath];
    }
    
    [self deleteRowsAtIndexPaths:ma withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.data removeAllObjects];
    
    [self endUpdates];
}

@end
