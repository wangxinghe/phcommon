//
//  SearchableTableViewDelegate.h
//  NESTAB
//
//  Created by Ricol Wang on 22/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SearchableTableViewController;

@protocol SearchableTableViewDelegate

- (void) searchableTableView:(SearchableTableViewController *)viewController didSelectRowAtIndexPath:(NSIndexPath *)path FromData:(NSArray *)data;

@end
