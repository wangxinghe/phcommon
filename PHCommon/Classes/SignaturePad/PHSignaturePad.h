//
//  PHSignaturePad.h
//  SignatureTest
//
//  Created by Joel Esler on 14/10/12. Updated By Ricol Wang
//

#import <UIKit/UIKit.h>
#import "PHSignaturePadDelegate.h"

@interface PHSignaturePad : UIView

@property (weak, nonatomic) id<PHSignaturePadDelegate> delegate;
@property (strong, nonatomic) UIColor *penColor;
@property (strong, nonatomic) NSNumber *penSize;

- (void)clearPad;
- (void)loadSignature:(NSArray *)signature;

@end
