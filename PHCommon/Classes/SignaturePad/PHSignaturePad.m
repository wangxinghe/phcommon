//
//  PHSignaturePad.m
//  SignatureTest
//
//  Created by Joel Esler on 14/10/12. Updated By Ricol Wang
//

#import "PHSignaturePad.h"

#define COLOR_PEN_DEFAULT [UIColor blueColor]
#define SIZE_PEN_DEFAULT 10

@interface PHSignaturePad ()

@property (strong) UIScrollView *scrollAncestor;
@property (strong, nonatomic) NSMutableArray *mutableArrayPath;
@property CGPoint prevTouchLocation;

@end

@implementation PHSignaturePad

@synthesize scrollAncestor = _scrollAncestor;
@synthesize delegate = _delegate;
@synthesize penColor = _penColor;
@synthesize penSize = _penSize;

#pragma mark - Override Methods

- (UIColor *)penColor
{
    if (!_penColor)
        _penColor = COLOR_PEN_DEFAULT;
    return _penColor;
}

- (NSNumber *)penSize
{
    if (!_penSize)
        _penSize = @(SIZE_PEN_DEFAULT);
    return _penSize;
}

- (void)setPenColor:(UIColor *)penColor
{
    _penColor = penColor;
    [self setNeedsDisplay];
}

- (void)setPenSize:(NSNumber *)penSize
{
    _penSize = penSize;
    [self setNeedsDisplay];
}

- (NSMutableArray *)mutableArrayPath
{
    if (!_mutableArrayPath)
        _mutableArrayPath = [NSMutableArray new];
    
    return _mutableArrayPath;
}

- (void)setDelegate:(id<PHSignaturePadDelegate>)delegate
{
    _delegate = delegate;
    [self setNeedsDisplay];     // display any data already contained in the delegate
}

- (void)drawRect:(CGRect)rect
{
    UIGraphicsBeginImageContext(self.bounds.size);
    
    //draw whatever to the image context
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //clear first
    CGContextSetFillColorWithColor(context, self.backgroundColor.CGColor);
    CGContextFillRect(context, self.bounds);
    
    //draw path
    CGContextSetLineWidth(context, self.penSize.intValue);
    CGContextSetLineCap(context, kCGLineCapRound);  // make the line ends round (necessary for 'dots' to appear properly)
    CGContextSetLineJoin(context, kCGLineJoinRound);    // make the line corners round (looks smoother)
    CGContextSetStrokeColorWithColor(context, self.penColor.CGColor);
    
    for (NSDictionary *coord in self.mutableArrayPath)
    {
        CGPoint moveTo = [self makeMoveToPointFromCoordinateObject:coord];
        CGPoint lineTo = [self makeLineToPointFromCoordinateObject:coord];
        if (moveTo.x == lineTo.x && moveTo.y == lineTo.y)
        {
            // lineTo and moveTo points are the same, so interpret as moveTo instruction
            CGContextMoveToPoint(context, moveTo.x, moveTo.y);
        } else
        {
            // interpret as lineTo instruction
            CGContextAddLineToPoint(context, lineTo.x, lineTo.y);
        }
    }
    
    // draw path
    CGContextStrokePath(context);
    
    UIImage *tmpImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [tmpImage drawInRect:self.bounds];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // resizing caused by orientation change will stretch drawn signature, so force redraw
    [self setNeedsDisplay];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_scrollAncestor != nil)
    {
        [_scrollAncestor setScrollEnabled:NO];
    }

    CGPoint touchLocation = [[touches anyObject] locationInView:self];
    [self.mutableArrayPath addObject:[self makeCoordinateObjectFromMoveTo:touchLocation lineTo:touchLocation]];
    _prevTouchLocation = touchLocation;
    
    [self setNeedsDisplay];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint touchLocation = [[touches anyObject] locationInView:self];
    [self.mutableArrayPath addObject:[self makeCoordinateObjectFromMoveTo:_prevTouchLocation lineTo:touchLocation]];
    _prevTouchLocation = touchLocation;
    
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_scrollAncestor != nil) { [_scrollAncestor setScrollEnabled:YES]; }
    
    CGPoint touchLocation = [[touches anyObject] locationInView:self];
    [self.mutableArrayPath addObject:[self makeCoordinateObjectFromMoveTo:_prevTouchLocation lineTo:touchLocation]];
    _prevTouchLocation = touchLocation;
    
    [_delegate PHSignaturePadSignatureUpdated:self.mutableArrayPath];
    [self setNeedsDisplay];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_scrollAncestor != nil) { [_scrollAncestor setScrollEnabled:YES]; }
    
    CGPoint touchLocation = [[touches anyObject] locationInView:self];
    [self.mutableArrayPath addObject:[self makeCoordinateObjectFromMoveTo:_prevTouchLocation lineTo:touchLocation]];
    _prevTouchLocation = touchLocation;
    
    [_delegate PHSignaturePadSignatureUpdated:self.mutableArrayPath];
    [self setNeedsDisplay];
}

#pragma mark - CGPoint and JSON serializable object conversion
/**
 Makes a JSON serializable data object from two CGPoints representing the moveTo and lineTo coordinates.
 */
- (NSDictionary *)makeCoordinateObjectFromMoveTo:(CGPoint)moveTo lineTo:(CGPoint)lineTo
{
    return @{
    @"mx":[NSNumber numberWithFloat:moveTo.x],
    @"my":[NSNumber numberWithFloat:moveTo.y],
    @"lx":[NSNumber numberWithFloat:lineTo.x],
    @"ly":[NSNumber numberWithFloat:lineTo.y]
    };
}

/**
 Extracts the moveTo coordinate from a data object.
 */
- (CGPoint)makeMoveToPointFromCoordinateObject:(NSDictionary *)coord
{
    float mx = [[coord valueForKey:@"mx"] floatValue];
    float my = [[coord valueForKey:@"my"] floatValue];
    return CGPointMake(mx, my);
}

/**
 Extracts the lineTo coordinate from a data object.
 */
- (CGPoint)makeLineToPointFromCoordinateObject:(NSDictionary *)coord
{
    float lx = [[coord valueForKey:@"lx"] floatValue];
    float ly = [[coord valueForKey:@"ly"] floatValue];
    return CGPointMake(lx, ly);
}

#pragma mark - Public Methods

- (void)clearPad
{
    [self.mutableArrayPath removeAllObjects];
    
    [_delegate PHSignaturePadSignatureUpdated:self.mutableArrayPath];
    [self setNeedsDisplay];
}

- (void)loadSignature:(NSArray *)signature
{
    [self.mutableArrayPath removeAllObjects];
    [self.mutableArrayPath addObjectsFromArray:signature];
    
    [self setNeedsDisplay];
}

@end
