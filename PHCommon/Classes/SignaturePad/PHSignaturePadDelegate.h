//
//  PHSignaturePadDelegate.h
//  AGL-Forms
//
//  Created by Philology on 15/10/12. Updated By Ricol Wang
//  Copyright (c) 2012 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
    SignatureCaptureDelegate acts as the data source for a SignatureCapture view.
 */
@protocol PHSignaturePadDelegate <NSObject>

/**
    The signature path is stored as an array of points.
    The signature path is stored as array of dictionary objects with keys "mx", "my", representing a moveTo coordinate, and "lx", and "ly", representing a moveTo coordinate.
 */


/**
    The signatureDidChange message is sent to the delegate when a new continuous line is completed or the when signature is cleared.
 */
- (void)PHSignaturePadSignatureUpdated:(NSArray *)arrayPath;

@end
