//
//  PHButtonWithExclusiveTouch.m
//  PHCustomControls
//
//  Created by Ricol Wang on 28/08/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import "PHButtonWithExclusiveTouch.h"

@implementation PHButtonWithExclusiveTouch

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.exclusiveTouch = YES;
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        self.exclusiveTouch = YES;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        self.exclusiveTouch = YES;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
