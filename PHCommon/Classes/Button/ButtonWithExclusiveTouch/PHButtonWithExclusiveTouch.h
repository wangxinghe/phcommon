//
//  PHButtonWithExclusiveTouch.h
//  PHCustomControls
//
//  Created by Ricol Wang on 28/08/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHButtonWithExclusiveTouch : UIButton

@end
