//
//  PHButtonWithLongPressGestureDelegate.h
//  PHCustomControls
//
//  Created by Ricol Wang on 28/08/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PHButtonWithLongPressGesture;

@protocol PHButtonWithLongPressGestureDelegate <NSObject>

- (void)PHButtonWithLongPressGestureDidLongPressed:(PHButtonWithLongPressGesture *)button;

@end
