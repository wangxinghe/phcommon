//
//  PHButtonWithLongPressGesture.h
//  PHCustomControls
//
//  Created by Ricol Wang on 28/08/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHButtonWithLongPressGestureDelegate.h"

@interface PHButtonWithLongPressGesture : UIButton

@property (weak) id <PHButtonWithLongPressGestureDelegate> delegate;

@end
