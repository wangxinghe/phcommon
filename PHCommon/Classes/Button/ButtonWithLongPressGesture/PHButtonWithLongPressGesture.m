//
//  PHButtonWithLongPressGesture.m
//  PHCustomControls
//
//  Created by Ricol Wang on 28/08/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import "PHButtonWithLongPressGesture.h"

@interface PHButtonWithLongPressGesture ()

@property UIGestureRecognizer *longPressGestureRecognizer;

- (void)initMembers;

@end

@implementation PHButtonWithLongPressGesture

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initMembers];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

#pragma mark - Private Methods

- (void)initMembers
{
    self.longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(HandleLongPressGesture:)];
    [self addGestureRecognizer:self.longPressGestureRecognizer];
}

- (void)HandleLongPressGesture:(UILongPressGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        [self.delegate PHButtonWithLongPressGestureDidLongPressed:self];
    }
}


@end
