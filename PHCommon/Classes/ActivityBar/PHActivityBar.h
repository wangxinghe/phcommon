//
//  PHActivityBar.h
//
//  Created by Ricol Wang

#import <UIKit/UIKit.h>

@interface PHActivityBar : UIView

+ (void) showWithText:(NSString *)text andDetails:(NSString *)details andTotalValue:(float)totalValue andDetailValue:(float)detailValue;
+ (void) dismiss;
+ (void) show;
+ (void) setWidthRatio:(float)ratio;

@end
