//
//  ZAActivityBar.m
//
//  Created by Zac Altman on 24/11/12.
//  Copyright (c) 2012 Zac Altman. All rights reserved.
//

#import "PHActivityBar.h"
#import "utils.h"

//#define _DEBUG

#define WINDOW_OFFSET -40
#define BAR_COLOR [[UIColor blackColor] colorWithAlphaComponent:0.5f]
#define RATIO_WIDTH 1
#define BAR_HEIGHT 35.0f
#define WIDTH_DETAIL_LABLE 50
#define RIGHT_MARGIN_DETAIL_LABEL 10
#define SPINNER_SIZE 24.0f
#define ICON_OFFSET (BAR_HEIGHT - SPINNER_SIZE) / 2.0f
#define TIME_ANIMATION 0.2

#ifndef kCFCoreFoundationVersionNumber_iOS_8_0
#define kCFCoreFoundationVersionNumber_iOS_8_0 1129.15
#endif

@interface PHActivityBar ()

@property BOOL isVisible;
@property NSUInteger offset;
@property (strong, nonatomic) NSNumber *widthRatio;

@property (strong) NSString *text;
@property (nonatomic, strong) UIView *actionIndicatorView;
@property (nonatomic, strong) UILabel *actionIndicatorLabel;
@property (nonatomic, strong) NSTimer *fadeOutTimer;
@property (nonatomic, weak) UIView *overlayWindow;
@property (nonatomic, strong) UIView *barView;
@property (nonatomic, strong) UILabel *stringLabel;
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, strong) UIActivityIndicatorView *spinnerView;
@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation PHActivityBar

@synthesize fadeOutTimer, overlayWindow, barView, stringLabel, detailLabel, spinnerView, imageView, actionIndicatorView, actionIndicatorLabel;

+ (PHActivityBar *) sharedView
{
    static dispatch_once_t once;
    static PHActivityBar *sharedView;
    dispatch_once(&once, ^ {
        sharedView = [[PHActivityBar alloc] initWithFrame:CGRectInset([utils getWindowRect], -WINDOW_OFFSET, -WINDOW_OFFSET)];
        sharedView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        sharedView.userInteractionEnabled = NO;
        sharedView.hidden = YES;
//        [[NSNotificationCenter defaultCenter] addObserver:sharedView selector:@selector(statusBarOrientationDidChange:)
//                                                     name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
                                           });
    sharedView.frame = CGRectInset([utils getWindowRect], -WINDOW_OFFSET, -WINDOW_OFFSET);
    return sharedView;
}

+ (void) setWidthRatio:(float)ratio
{
    [PHActivityBar sharedView].widthRatio = (ratio <= 0 || ratio > 1) ? @(RATIO_WIDTH) : @(ratio);
}

#pragma mark - Public static Methods

+ (void)show
{
    if ([PHActivityBar sharedView].hidden)
    {
//        [[PHActivityBar sharedView] setTransformForCurrentOrientation];
        
        [PHActivityBar sharedView].alpha = 0;
        [PHActivityBar sharedView].hidden = NO;
        
        if ([PHActivityBar sharedView].superview) return;
        [[PHActivityBar sharedView].overlayWindow addSubview:[PHActivityBar sharedView]];
        
        [UIView animateWithDuration:TIME_ANIMATION animations:^(){
            [PHActivityBar sharedView].alpha = 1;
        } completion:^(BOOL finished){
            
        }];
    }
}

+ (void) dismiss
{
    if ([PHActivityBar sharedView].hidden) return;
    
    [UIView animateWithDuration:TIME_ANIMATION animations:^(){
        [PHActivityBar sharedView].alpha = 0;
    } completion:^(BOOL finished){
        [PHActivityBar sharedView].hidden = YES;
        [[PHActivityBar sharedView].spinnerView stopAnimating];
        [[PHActivityBar sharedView] removeFromSuperview];
    }];
}

+ (void) showWithText:(NSString *)text andDetails:(NSString *)details andTotalValue:(float)totalValue andDetailValue:(float)detailValue
{
    [[PHActivityBar sharedView].spinnerView startAnimating];
    NSMutableString *tmpMutableString = [[NSMutableString alloc] init];
    
    if (text)
        [PHActivityBar sharedView].text = [text copy];
    
    if ([PHActivityBar sharedView].text)
        [tmpMutableString appendString:[PHActivityBar sharedView].text];
    
    if (totalValue >= 0 && totalValue <= 1)
    {
        [PHActivityBar sharedView].detailLabel.text = [NSString stringWithFormat:@"%2.0f%%", totalValue * 100];
    }
    
    if (details)
        [tmpMutableString appendFormat:@" %@", details];
    
    if (detailValue >= 0 && detailValue <= 1)
        [tmpMutableString appendFormat:@" %2.0f%%", detailValue * 100];
    
    [PHActivityBar sharedView].stringLabel.text = tmpMutableString;
    
    if (totalValue >= 0 && totalValue <= 1)
        [PHActivityBar sharedView].detailLabel.text = [NSString stringWithFormat:@"%2.0f%%", totalValue * 100];
    else
        [PHActivityBar sharedView].detailLabel.text = @"";
    
    [PHActivityBar show];
}

#pragma mark - Private Methods

//- (BOOL)shouldPerformOrientationTransform {
//    BOOL isPreiOS8 = NSFoundationVersionNumber < kCFCoreFoundationVersionNumber_iOS_8_0;
//    // prior to iOS8 code needs to take care of rotation if it is being added to the window
//    return isPreiOS8 && [self.superview isKindOfClass:[UIWindow class]];
//}
//
//- (void)statusBarOrientationDidChange:(NSNotification *)notification {
//    UIView *superview = self.superview;
//    if (!superview) {
//        return;
//    } else if ([self shouldPerformOrientationTransform]) {
//        [self setTransformForCurrentOrientation];
//    } else {
//        self.frame = self.superview.bounds;
//        [self setNeedsDisplay];
//    }
//}
//
//- (void)setTransformForCurrentOrientation {
//    // Stay in sync with the superview
//    if (self.superview) {
//        self.bounds = self.superview.bounds;
//        [self setNeedsDisplay];
//    }
//    
//    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
//    CGFloat radians = 0;
//    if (UIInterfaceOrientationIsLandscape(orientation)) {
//        if (orientation == UIInterfaceOrientationLandscapeLeft) { radians = -(CGFloat)M_PI_2; }
//        else { radians = (CGFloat)M_PI_2; }
//        // Window coordinates differ!
//        self.bounds = CGRectMake(0, 0, self.bounds.size.height, self.bounds.size.width);
//    } else {
//        if (orientation == UIInterfaceOrientationPortraitUpsideDown) { radians = (CGFloat)M_PI; }
//        else { radians = 0; }
//    }
//    CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(radians);
//    [self setTransform:rotationTransform];
//}

#pragma mark - Getters

- (UIView *) actionIndicatorView
{
    if (!actionIndicatorView)
    {
        
        float size = BAR_HEIGHT / 2;
        CGRect rect = CGRectZero;
        rect.size = CGSizeMake(size, size);
        rect.origin.y = (BAR_HEIGHT - size) / 2;
        rect.origin.x = self.barView.bounds.size.width - size - rect.origin.y;
        
        actionIndicatorView = [UIView new];
#ifdef _DEBUG
        actionIndicatorView.backgroundColor = [utils randomColor];
#else
		actionIndicatorView.backgroundColor = [UIColor clearColor];
#endif
        actionIndicatorView.alpha = 0.0f;
        actionIndicatorView.frame = rect;
        actionIndicatorView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin);
        
        // Circle shape
        CAShapeLayer *pill = [CAShapeLayer new];
        pill.path = CGPathCreateWithEllipseInRect(actionIndicatorView.bounds, nil);
        pill.fillColor = [UIColor whiteColor].CGColor;
        [actionIndicatorView.layer addSublayer:pill];
    }
    
    if(!actionIndicatorView.superview)
        [self.barView addSubview:actionIndicatorView];
    
    return actionIndicatorView;
}

- (UILabel *) actionIndicatorLabel
{
    if (!actionIndicatorLabel)
    {
        actionIndicatorLabel = [[UILabel alloc] initWithFrame:CGRectZero];
		actionIndicatorLabel.textColor = [UIColor blackColor];
#ifdef _DEBUG
        actionIndicatorLabel.backgroundColor = [utils randomColor];
#else
		actionIndicatorLabel.backgroundColor = [UIColor clearColor];
#endif
		actionIndicatorLabel.adjustsFontSizeToFitWidth = YES;
		actionIndicatorLabel.textAlignment = NSTextAlignmentCenter;
		actionIndicatorLabel.font = [UIFont boldSystemFontOfSize:12];
        actionIndicatorLabel.numberOfLines = 0;
    }
    
    if(!actionIndicatorLabel.superview)
        [self.actionIndicatorView addSubview:actionIndicatorLabel];
    
    return actionIndicatorLabel;
}

- (UILabel *)stringLabel
{
    if (!stringLabel)
    {
        stringLabel = [[UILabel alloc] initWithFrame:CGRectMake(2 * ICON_OFFSET + SPINNER_SIZE, 0, self.barView.bounds.size.width - (2 * ICON_OFFSET + SPINNER_SIZE + WIDTH_DETAIL_LABLE + RIGHT_MARGIN_DETAIL_LABEL), BAR_HEIGHT)];
		stringLabel.textColor = [UIColor whiteColor];
#ifdef _DEBUG
        stringLabel.backgroundColor = [utils randomColor];
#else
		stringLabel.backgroundColor = [UIColor clearColor];
#endif
		stringLabel.adjustsFontSizeToFitWidth = YES;
		stringLabel.textAlignment = NSTextAlignmentLeft;
		stringLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        stringLabel.autoresizingMask = (UIViewAutoresizingFlexibleWidth);
		stringLabel.font = [UIFont boldSystemFontOfSize:14];
		stringLabel.shadowColor = [UIColor blackColor];
		stringLabel.shadowOffset = CGSizeMake(0, -1);
        stringLabel.numberOfLines = 5;
    }
    
    if(!stringLabel.superview)
    {
        [self.barView addSubview:stringLabel];
    }
    
    return stringLabel;
}

- (UILabel *)detailLabel
{
    if (!detailLabel)
    {
        detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.barView.bounds.size.width - WIDTH_DETAIL_LABLE - RIGHT_MARGIN_DETAIL_LABEL, 0, WIDTH_DETAIL_LABLE, BAR_HEIGHT)];
		detailLabel.textColor = [UIColor whiteColor];
#ifdef _DEBUG
        detailLabel.backgroundColor = [utils randomColor];
#else
		detailLabel.backgroundColor = [UIColor clearColor];
#endif
		detailLabel.adjustsFontSizeToFitWidth = YES;
		detailLabel.textAlignment = NSTextAlignmentRight;
		detailLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        detailLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
		detailLabel.font = [UIFont boldSystemFontOfSize:14];
		detailLabel.shadowColor = [UIColor blackColor];
		detailLabel.shadowOffset = CGSizeMake(0, -1);
        detailLabel.numberOfLines = 0;
    }
    
    if(!detailLabel.superview)
        [self.barView addSubview:detailLabel];
    
    return detailLabel;
}

- (UIView *)overlayWindow
{
    if(!overlayWindow)
    {
        overlayWindow = [[[UIApplication sharedApplication] delegate] window].rootViewController.view;
        NSLog(@"overLaywindow: %@", overlayWindow);
    }
    return overlayWindow;
}

- (UIView *)barView
{
    if(!barView)
    {
        CGRect tmpRect = self.bounds;
        
        tmpRect.origin.y = self.bounds.size.height - 10;
        float width = self.widthRatio.floatValue * self.bounds.size.width;
        tmpRect.origin.x = (self.bounds.size.width - width) / 2;
        tmpRect.size.width = width;
        
        tmpRect.size.height = BAR_HEIGHT;
        barView = [[UIView alloc] initWithFrame:tmpRect];
        barView.layer.cornerRadius = 6;
#ifdef _DEBUG
        barView.backgroundColor = [utils randomColor];
#else
		barView.backgroundColor = BAR_COLOR;
#endif
        barView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
        [self addSubview:barView];
    }
    return barView;
}

- (UIActivityIndicatorView *)spinnerView
{
    if (!spinnerView)
    {
        spinnerView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		spinnerView.hidesWhenStopped = YES;
#ifdef _DEBUG
        spinnerView.backgroundColor = [utils randomColor];
#else
		spinnerView.backgroundColor = [UIColor clearColor];
#endif
		spinnerView.frame = CGRectMake(ICON_OFFSET, ICON_OFFSET, SPINNER_SIZE, SPINNER_SIZE);
    }
    
    if(!spinnerView.superview)
        [self.barView addSubview:spinnerView];
    
    return spinnerView;
}

- (UIImageView *)imageView
{
    if (!imageView)
    {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(ICON_OFFSET, ICON_OFFSET, SPINNER_SIZE, SPINNER_SIZE)];
#ifdef _DEBUG
        imageView.backgroundColor = [utils randomColor];
#else
		imageView.backgroundColor = [UIColor clearColor];
#endif
    }
    
    if(!imageView.superview)
        [self.barView addSubview:imageView];
    
    return imageView;
}

- (NSNumber *)widthRatio
{
    if (!_widthRatio)
    {
        _widthRatio = @(RATIO_WIDTH);
    }
    
    return _widthRatio;
}

@end
