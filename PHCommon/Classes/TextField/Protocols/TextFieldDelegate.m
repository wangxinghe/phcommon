//
//  TextFieldDelegate.m
//  NESTAB
//
//  Created by ricolwang on 7/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "TextFieldDelegate.h"
#import "Validator.h"
#import "NumberValidator.h"
#import "EmailValidator.h"
#import "MobilePhoneNumberValidator.h"
#import "HomePhoneValidator.h"
#import "ValidatedTextField.h"
#import "MandatoryValidator.h"

@implementation TextFieldDelegate

- (id)initWithType:(TextFieldType)textFieldType
{
    self = [super init];
    
    if (self)
    {
        self.textFieldType = textFieldType;
        self.nextDelegate = nil;
    }
    
    return self;
}

- (BOOL)ValidateText:(UITextField *)textField andText:(NSString *)text
{
    Validator *tempValidator = nil;
    
    if (self.textFieldType == TEXTFIELD_TYPE_EMAIL)
        tempValidator = [[EmailValidator alloc] init];
    else if (self.textFieldType == TEXTFIELD_TYPE_MOBILEPHONE)
        tempValidator = [[MobilePhoneNumberValidator alloc] init];
    else if (self.textFieldType == TEXTFIELD_TYPE_HOMEPHONE)
        tempValidator = [[HomePhoneValidator alloc] init];
    else if (self.textFieldType == TEXTFIELD_TYPE_NUMBER)
        tempValidator = [[NumberValidator alloc] init];
    else if (self.textFieldType == TEXTFILED_TYPE_MANDOTORY)
        tempValidator = [[MandatoryValidator alloc] init];
    else
        return YES;

    return [tempValidator isValid:text];
}

#pragma mark - UITextFieldDelegate

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    BOOL result = [self ValidateText:textField andText:textField.text];
    ValidatedTextField *tempTextField = (ValidatedTextField *)textField;
    tempTextField.backgroundColor = (result ? [UIColor whiteColor] : [UIColor colorWithRed:1 green:0 blue:0 alpha:0.2]);
    tempTextField.valid = result;
    
    if (self.nextDelegate && [self.nextDelegate respondsToSelector:@selector(textFieldDidEndEditing:)])
        [self.nextDelegate textFieldDidEndEditing:textField];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL confirm = YES;
    
    if (self.nextDelegate && [self.nextDelegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)])
        confirm = [self.nextDelegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
    
    if (confirm)
    {
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        BOOL result = [self ValidateText:textField andText:newText];
        ValidatedTextField *tempTextField = (ValidatedTextField *)textField;
        tempTextField.backgroundColor = (result ? [UIColor whiteColor] : [UIColor colorWithRed:1 green:0 blue:0 alpha:0.2]);
        tempTextField.textColor = (result ? [UIColor blackColor] : [UIColor whiteColor]);
        tempTextField.valid = result;
    }
    
    return confirm;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.nextDelegate && [self.nextDelegate respondsToSelector:@selector(textFieldShouldBeginEditing:)])
    {
        return [self.nextDelegate textFieldShouldBeginEditing:textField];
    }else
        return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.nextDelegate && [self.nextDelegate respondsToSelector:@selector(textFieldDidBeginEditing:)])
        return [self.nextDelegate textFieldDidBeginEditing:textField];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (self.nextDelegate && [self.nextDelegate respondsToSelector:@selector(textFieldShouldEndEditing:)])
        return [self.nextDelegate textFieldShouldEndEditing:textField];
    else
        return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (self.nextDelegate && [self.nextDelegate respondsToSelector:@selector(textFieldShouldClear:)])
        return [self.nextDelegate textFieldShouldClear:textField];
    else
        return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.nextDelegate && [self.nextDelegate respondsToSelector:@selector(textFieldShouldReturn:)])
        return [self.nextDelegate textFieldShouldReturn:textField];
    else
        return YES;
}

@end
