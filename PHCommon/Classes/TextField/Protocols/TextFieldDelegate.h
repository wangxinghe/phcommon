//
//  TextFieldDelegate.h
//  NESTAB
//
//  Created by ricolwang on 7/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    TEXTFIELD_TYPE_EMAIL, TEXTFIELD_TYPE_MOBILEPHONE, TEXTFIELD_TYPE_HOMEPHONE, TEXTFIELD_TYPE_NUMBER, TEXTFILED_TYPE_MANDOTORY
} TextFieldType;

@interface TextFieldDelegate : NSObject <UITextFieldDelegate>

@property (nonatomic) TextFieldType textFieldType;
@property (nonatomic, weak) id <UITextFieldDelegate> nextDelegate;

- (id)  initWithType:(TextFieldType)textFieldType;
- (BOOL)ValidateText:(UITextField *)textField andText:(NSString *)text;

@end