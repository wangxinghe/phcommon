//
//  MandatoryTextField.m
//  NESTAB
//
//  Created by Philology on 31/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "MandatoryTextField.h"

@implementation MandatoryTextField

-(id)init
{
    self = [super init];
    if (self)
    {
        self.textFieldDelegate = [[TextFieldDelegate alloc] initWithType:TEXTFILED_TYPE_MANDOTORY];
        self.delegate = self.textFieldDelegate;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.textFieldDelegate = [[TextFieldDelegate alloc] initWithType:TEXTFILED_TYPE_MANDOTORY];
        self.delegate = self.textFieldDelegate;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.textFieldDelegate = [[TextFieldDelegate alloc] initWithType:TEXTFILED_TYPE_MANDOTORY];
        self.delegate = self.textFieldDelegate;
    }
    return self;
}

@end
