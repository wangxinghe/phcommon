//
//  MandatoryValidator.m
//  NESTAB
//
//  Created by Philology on 31/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "MandatoryValidator.h"

@implementation MandatoryValidator

- (BOOL)isValid:(NSString *)Text
{
    if (Text == nil) return NO;
    else if ([Text isEqualToString:@""]) return NO;
    else return YES;
}

@end
