//
//  NumberValidator.m
//  NESTAB
//
//  Created by ricolwang on 9/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "NumberValidator.h"

@implementation NumberValidator

- (BOOL)isValid:(NSString *)Text
{
    BOOL bResult = YES;
    
    if (Text == nil) return NO;
    else if ([Text isEqualToString:@""]) return YES;
    else
    {
        NSCharacterSet *validChars = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
        
        
        for (unsigned i = 0; i < Text.length; i++)
        {
            unichar c = [Text characterAtIndex:i];
            if ([validChars characterIsMember:c] == NO)
            {
                bResult = NO;
                break;
            }
        }
        
        return bResult;
    }
}

@end
