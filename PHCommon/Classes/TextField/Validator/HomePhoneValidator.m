//
//  HomePhoneValidator.m
//  NESTAB
//
//  Created by ricolwang on 9/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "HomePhoneValidator.h"

@implementation HomePhoneValidator

- (BOOL)isValid:(NSString *)Text
{
    if (Text == nil) return NO;
    else if ([Text isEqualToString:@""]) return YES;
    else
    {
        NSCharacterSet *validChars = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
        NSCharacterSet *digits = [NSCharacterSet decimalDigitCharacterSet];
        
        int numDigits = 0;
        
        for (unsigned i = 0; i < Text.length; i++) {
            unichar c = [Text characterAtIndex:i];
            if ([validChars characterIsMember:c] == NO) { return NO; }
            
            if ([digits characterIsMember:c]) { numDigits++; }
        }
        
        return (numDigits == 10);
    }
}

@end
