//
//  EmailValidator.m
//  NESTAB
//
//  Created by ricolwang on 9/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "EmailValidator.h"

@implementation EmailValidator

- (BOOL)isValid:(NSString *)Text
{
    if (Text == nil) return NO;
    else if ([Text isEqualToString:@""]) return YES;
    else
    {
        NSError *error = nil;
        NSRegularExpression *emailRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}"
                                                                                    options:NSRegularExpressionCaseInsensitive
                                                                                      error:&error];
        NSUInteger numMatches = [emailRegex numberOfMatchesInString:Text options:0 range:NSMakeRange(0, [Text length])];
        
        return numMatches > 0;
    }
}

@end
