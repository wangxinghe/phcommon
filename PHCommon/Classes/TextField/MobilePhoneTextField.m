//
//  MobilePhoneTextField.m
//  NESTAB
//
//  Created by ricolwang on 7/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "MobilePhoneTextField.h"

@implementation MobilePhoneTextField

-(id)init
{
    self = [super init];
    if (self)
    {
        self.textFieldDelegate = [[TextFieldDelegate alloc] initWithType:TEXTFIELD_TYPE_MOBILEPHONE];
        self.delegate = self.textFieldDelegate;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.textFieldDelegate = [[TextFieldDelegate alloc] initWithType:TEXTFIELD_TYPE_MOBILEPHONE];
        self.delegate = self.textFieldDelegate;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.textFieldDelegate = [[TextFieldDelegate alloc] initWithType:TEXTFIELD_TYPE_MOBILEPHONE];
        self.delegate = self.textFieldDelegate;
    }
    return self;
}

@end
