//
//  MandatoryTextField.h
//  NESTAB
//
//  Created by Philology on 31/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "ValidatedTextField.h"

@interface MandatoryTextField : ValidatedTextField

@end
