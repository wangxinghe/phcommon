//
//  ValidatedTextField.h
//  NESTAB
//
//  Created by ricolwang on 7/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldDelegate.h"

@interface ValidatedTextField : UITextField

@property (nonatomic) BOOL valid;
@property (nonatomic, strong) TextFieldDelegate *textFieldDelegate;

@end
