//
//  ValidatedTextField.m
//  NESTAB
//
//  Created by ricolwang on 7/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "ValidatedTextField.h"

@implementation ValidatedTextField

-(id)init
{
    self = [super init];
    if (self)
    {
        self.valid = YES;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.valid = YES;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.valid = YES;
    }
    return self;
}

- (BOOL)valid
{
    BOOL tempResult = [self.textFieldDelegate ValidateText:self andText:self.text];
    
    self.valid = tempResult;
    self.backgroundColor = (tempResult ? [UIColor whiteColor] : [UIColor colorWithRed:1.0 green:0 blue:0.0 alpha:0.3]);
    
    return tempResult;
}

-(void)setText:(NSString *)text
{
    [super setText:text];
    
    BOOL tempResult = [self.textFieldDelegate ValidateText:self andText:self.text];
    
    self.valid = tempResult;
    self.backgroundColor = (tempResult ? [UIColor whiteColor] : [UIColor colorWithRed:1.0 green:0 blue:0.0 alpha:0.3]);
}

-(void)setDelegate:(id<UITextFieldDelegate>)delegate
{
    [super setDelegate:self.textFieldDelegate];
    
    if (self.textFieldDelegate) {
        if (![delegate isEqual:self.textFieldDelegate])
            self.textFieldDelegate.nextDelegate = delegate;
    }
    else {
        NSLog(@"textFieldDelegate is nil");
	}
}

@end
