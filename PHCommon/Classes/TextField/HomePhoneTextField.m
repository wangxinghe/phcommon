//
//  HomePhoneTextField.m
//  NESTAB
//
//  Created by ricolwang on 7/01/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "HomePhoneTextField.h"

@implementation HomePhoneTextField

-(id)init
{
    self = [super init];
    if (self)
    {
        self.textFieldDelegate = [[TextFieldDelegate alloc] initWithType:TEXTFIELD_TYPE_HOMEPHONE];
        self.delegate = self.textFieldDelegate;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.textFieldDelegate = [[TextFieldDelegate alloc] initWithType:TEXTFIELD_TYPE_HOMEPHONE];
        self.delegate = self.textFieldDelegate;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.textFieldDelegate = [[TextFieldDelegate alloc] initWithType:TEXTFIELD_TYPE_HOMEPHONE];
        self.delegate = self.textFieldDelegate;
    }
    return self;
}

@end
