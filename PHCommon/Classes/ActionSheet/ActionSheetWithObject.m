//
//  ActionSheetWithObject.m
//  PHCustomControls
//
//  Created by Ricol Wang on 20/09/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import "ActionSheetWithObject.h"

@implementation ActionSheetWithObject

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
