//
//  ActionSheetWithObject.h
//  PHCustomControls
//
//  Created by Ricol Wang on 20/09/13.
//  Copyright (c) 2013 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionSheetWithObject : UIActionSheet

@property (strong) NSObject *object;

@end
